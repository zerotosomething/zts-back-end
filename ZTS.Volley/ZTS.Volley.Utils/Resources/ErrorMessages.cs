﻿namespace ZTS.Volley.Utils.Resources
{
    static public class ErrorMessages
    {
        static public string GetServiceKeyNotFound(int id, string serviceName)
        {
            return string.Format(ValidatorMessagesResource.KeyNotFoundMessage, id, RemoveServiceSuffix(serviceName));
        }

        static public string GetServiceKeyNotFound(Guid id, string serviceName)
        {
            return string.Format(ValidatorMessagesResource.KeyNotFoundMessage, id, RemoveServiceSuffix(serviceName));
        }

        static private string RemoveServiceSuffix(string serviceName)
        {
            return serviceName.Replace("Service", "");
        }
    }
}
