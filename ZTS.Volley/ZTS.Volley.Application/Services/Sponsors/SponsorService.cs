﻿using AutoMapper;
using ZTS.Volley.Application.Dtos.Sponsors;
using ZTS.Volley.Application.Services.AzureStorage;
using ZTS.Volley.Application.Services.Sponsors.Interfaces;
using ZTS.Volley.Domain.Entities.Sponsors;
using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Infrastructure.Repositories.Sponsors.Interfaces;
using ZTS.Volley.Infrastructure.Repositories.Staff;

namespace ZTS.Volley.Application.Services.Sponsors
{
    public class SponsorService : ISponsorService
    {
        private readonly ISponsorRepository sponsorsRepository;
        private readonly IAzureStorageService storageService;
        private readonly IMapper mapper;

        public SponsorService(ISponsorRepository sponsorsRepository, IMapper mapper, IAzureStorageService storageService)
        {
            this.sponsorsRepository = sponsorsRepository;
            this.mapper = mapper;
            this.storageService = storageService;
        }

        public async Task<ResponseSponsorDto> Create(SponsorDto sponsorDto)
        {
            var logo = sponsorDto.Logo;

            sponsorDto.Logo = string.Empty;
            var entity = mapper.Map<Sponsor>(sponsorDto);
            var sponsor = await sponsorsRepository.Create(entity);
            var blob = await storageService.UploadAsync(logo, sponsor.Id);

            sponsor.Logo = blob.Blob.Uri.Split('?').First();
            await sponsorsRepository.Update(sponsor);

            return mapper.Map<ResponseSponsorDto>(sponsor);
        }

        public async Task Delete(Guid id)
        {
            var sponsor = await sponsorsRepository.GetById(id);

            if (sponsor is null)
            {
                throw new KeyNotFoundException();
            }

            var imageURL = await storageService.StoredImagesUrlAsync(id);
            var blobName = imageURL.First().Split('/').Last();
            await storageService.DeleteAsync(blobName);

            await sponsorsRepository.Delete(sponsor);
        }

        public async Task<List<ResponseSponsorDto>> GetAll()
        {
            var sponsors = await sponsorsRepository.GetAll();
            foreach (var sponsor in sponsors)
            {
                sponsor.Logo = storageService.GetImagesUrlAsync(sponsor.Id).Result.FirstOrDefault();
            }

            return mapper.Map<List<ResponseSponsorDto>>(sponsors);
        }

        public async Task<ResponseSponsorDto> GetById(Guid id)
        {
            var sponsor = await sponsorsRepository.GetById(id);

            if (sponsor is null)
            {
                throw new KeyNotFoundException();
            }

            sponsor.Logo = storageService.GetImagesUrlAsync(id).Result.First();

            return mapper.Map<ResponseSponsorDto>(sponsor);
        }

        public async Task<List<ResponseSponsorDto>> GetById(List<Guid> ids)
        {
            var sponsors = await sponsorsRepository.GetById(ids);

            foreach (var sponsor in sponsors)
            {
                sponsor.Logo = storageService.GetImagesUrlAsync(sponsor.Id).Result.First();
            }

            return mapper.Map<List<ResponseSponsorDto>>(sponsors);
        }

        public async Task<ResponseSponsorDto> Update(Guid id, SponsorDto sponsorDto)
        {
            var sponsor = await sponsorsRepository.GetById(id);

            if (sponsor is null)
            {
                throw new KeyNotFoundException();
            }

            mapper.Map(sponsorDto, sponsor);
            await sponsorsRepository.Update(sponsor);

            if (!sponsor.Logo.Contains("zts.blob"))
            {
                var imageURL = await storageService.StoredImagesUrlAsync(id);
                var blobName = imageURL.First().Split('/').Last();
                await storageService.DeleteAsync(blobName);
            
                await storageService.UploadAsync(sponsorDto.Logo, sponsor.Id);
                sponsor.Logo = storageService.StoredImagesUrlAsync(sponsor.Id).Result.First();
                await sponsorsRepository.Update(sponsor);
                sponsor.Logo = storageService.GetImagesUrlAsync(sponsor.Id).Result.First();
            }
            else if(sponsor.Logo.Contains("zts.blob"))
            {
                sponsor.Logo = sponsorDto.Logo.Split('?').First();
                await sponsorsRepository.Update(sponsor);
            }

            return mapper.Map<ResponseSponsorDto>(sponsor);
        }
    }
}
