﻿using ZTS.Volley.Application.Dtos.Sponsors;

namespace ZTS.Volley.Application.Services.Sponsors.Interfaces
{
    public interface ISponsorService
    {
        Task<ResponseSponsorDto> Create(SponsorDto sponsorDto);
        Task<ResponseSponsorDto> GetById(Guid id);
        Task<List<ResponseSponsorDto>> GetById(List<Guid> ids);
        Task<List<ResponseSponsorDto>> GetAll();
        Task<ResponseSponsorDto> Update(Guid id, SponsorDto sponsorDto);
        Task Delete(Guid id);
    }
}
