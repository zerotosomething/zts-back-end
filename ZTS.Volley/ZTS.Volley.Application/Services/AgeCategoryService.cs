﻿using AutoMapper;
using ZTS.Volley.Application.Dtos;
using ZTS.Volley.Application.Services.Interfaces;
using ZTS.Volley.Domain.Entities;
using ZTS.Volley.Infrastructure.Repositories.Interfaces;
using ZTS.Volley.Utils.Resources;

namespace ZTS.Volley.Application.Services
{
    public class AgeCategoryService : IAgeCategoryService
    {
        private readonly IAgeCategoryRepository ageCategoryRepository;
        private readonly IMapper mapper;

        public AgeCategoryService(IAgeCategoryRepository ageCategoryRepository, IMapper mapper)
        {
            this.ageCategoryRepository = ageCategoryRepository;
            this.mapper = mapper;
        }

        public async Task<ResponseAgeCategoryDto> Create(AgeCategoryDto ageCategoryDto)
        {
            var entity = mapper.Map<AgeCategory>(ageCategoryDto);
            var ageCategory = await ageCategoryRepository.Create(entity);

            return mapper.Map<ResponseAgeCategoryDto>(ageCategory);
        }

        public async Task Delete(int id)
        {
            var ageCategory = await ageCategoryRepository.GetById(id);

            if (ageCategory is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(AgeCategory)));
            }

            await ageCategoryRepository.Delete(ageCategory);
        }

        public async Task<List<ResponseAgeCategoryDto>> GetAll()
        {
            var ageCategories = await ageCategoryRepository.GetAll();

            return mapper.Map<List<ResponseAgeCategoryDto>>(ageCategories);
        }

        public async Task<ResponseAgeCategoryDto> GetById(int id)
        {
            var ageCategory = await ageCategoryRepository.GetById(id);

            if (ageCategory is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(AgeCategory)));
            }

            return mapper.Map<ResponseAgeCategoryDto>(ageCategory);
        }

        public async Task<List<ResponseAgeCategoryDto>> GetById(List<int> ids)
        {
            var ageCategories = await ageCategoryRepository.GetById(ids);

            return mapper.Map<List<ResponseAgeCategoryDto>>(ageCategories);
        }

        public async Task<ResponseAgeCategoryDto> Update(int id, AgeCategoryDto ageCategoryDto)
        {
            var ageCategory = await ageCategoryRepository.GetById(id);

            if (ageCategory is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(AgeCategory)));
            }

            mapper.Map(ageCategoryDto, ageCategory);
            await ageCategoryRepository.Update(ageCategory);

            return mapper.Map<ResponseAgeCategoryDto>(ageCategory);
        }
    }
}
