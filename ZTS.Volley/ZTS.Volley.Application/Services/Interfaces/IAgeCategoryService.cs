﻿using ZTS.Volley.Application.Dtos;

namespace ZTS.Volley.Application.Services.Interfaces
{
    public interface IAgeCategoryService
    {
        Task<ResponseAgeCategoryDto> Create(AgeCategoryDto ageCategoryDto);
        Task<ResponseAgeCategoryDto> GetById(int id);
        Task<List<ResponseAgeCategoryDto>> GetById(List<int> ids);
        Task<List<ResponseAgeCategoryDto>> GetAll();
        Task<ResponseAgeCategoryDto> Update(int id, AgeCategoryDto ageCategoryDto);
        Task Delete(int id);
    }
}
