﻿using ZTS.Volley.Application.Dtos.NewsArticles;

namespace ZTS.Volley.Application.Services.Interfaces.NewsArticles
{
    public interface IContentTypeService
    {
        Task<ResponseContentTypeDto> Create(ContentTypeDto contentTypeDto);
        Task<ResponseContentTypeDto> GetById(int id);
        Task<List<ResponseContentTypeDto>> GetById(List<int> ids);
        Task<List<ResponseContentTypeDto>> GetAll();
        Task<ResponseContentTypeDto> Update(int id, ContentTypeDto contentTypeDto);
        Task Delete(int id);
    }
}
