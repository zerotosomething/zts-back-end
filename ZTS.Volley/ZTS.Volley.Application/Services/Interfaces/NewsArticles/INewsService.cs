﻿using ZTS.Volley.Application.Dtos.NewsArticles;

namespace ZTS.Volley.Application.Services.Interfaces.NewsArticles
{
    public interface INewsService
    {
        Task<CreateNewsRequestDto> Create(CreateNewsRequestDto newsDto);
        Task<ResponseNewsDto> GetById(Guid id);
        Task<List<ResponseNewsDto>> GetByIds(List<Guid> ids);
        Task<List<ResponseNewsDto>> GetAll();
        Task<ResponseNewsDto> Update(Guid id, CreateNewsRequestDto newsDto);
        Task Delete(Guid id);
    }
}
