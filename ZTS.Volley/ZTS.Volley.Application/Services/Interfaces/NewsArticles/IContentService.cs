﻿using ZTS.Volley.Application.Dtos.NewsArticles;

namespace ZTS.Volley.Application.Services.Interfaces.NewsArticles
{
    public interface IContentService
    {
        Task<ResponseContentDto> Create(ContentDto contentDto);
        Task<ResponseContentDto> GetById(Guid id);
        Task<List<ResponseContentDto>> GetById(List<Guid> ids);
        Task<List<ResponseContentDto>> GetAll();
        Task<ResponseContentDto> Update(Guid id, ContentDto contentDto);
        Task Delete(Guid id);
    }
}
