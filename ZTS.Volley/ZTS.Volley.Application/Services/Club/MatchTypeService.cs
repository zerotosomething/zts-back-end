﻿using AutoMapper;
using ZTS.Volley.Application.Dtos.Club;
using ZTS.Volley.Application.Services.Club.Interfaces;
using ZTS.Volley.Infrastructure.Repositories.Club.Interfaces;
using MatchType = ZTS.Volley.Domain.Entities.Club.MatchType;


namespace ZTS.Volley.Application.Services.Club
{
    public class MatchTypeService : IMatchTypeService
    {
        private readonly IMatchTypeRepository matchTypeRepository;
        private readonly IMapper mapper;

        public MatchTypeService(IMatchTypeRepository matchTypeRepository, IMapper mapper)
        {
            this.matchTypeRepository = matchTypeRepository;
            this.mapper = mapper;
        }

        public async Task<ResponseMatchTypeDto> Create(MatchTypeDto matchTypeDto)
        {
            var entity = mapper.Map<MatchType>(matchTypeDto);
            var matchType = await matchTypeRepository.Create(entity);

            return mapper.Map<ResponseMatchTypeDto>(matchType);
        }

        public async Task Delete(int id)
        {
            var matchType = await matchTypeRepository.GetById(id);

            if (matchType is null)
            {
                throw new KeyNotFoundException();
            }

            await matchTypeRepository.Delete(matchType);
        }

        public async Task<List<ResponseMatchTypeDto>> GetAll()
        {
            var matchTypes = await matchTypeRepository.GetAll();

            return mapper.Map<List<ResponseMatchTypeDto>>(matchTypes);
        }

        public async Task<ResponseMatchTypeDto> GetById(int id)
        {
            var matchType = await matchTypeRepository.GetById(id);

            if (matchType is null)
            {
                throw new KeyNotFoundException();
            }

            return mapper.Map<ResponseMatchTypeDto>(matchType);
        }

        public async Task<List<ResponseMatchTypeDto>> GetById(List<int> ids)
        {
            var matchTypes = await matchTypeRepository.GetById(ids);

            return mapper.Map<List<ResponseMatchTypeDto>>(matchTypes);
        }

        public async Task<ResponseMatchTypeDto> Update(int id, MatchTypeDto matchTypeDto)
        {
            var matchType = await matchTypeRepository.GetById(id);

            if (matchType is null)
            {
                throw new KeyNotFoundException();
            }

            mapper.Map(matchTypeDto, matchType);
            await matchTypeRepository.Update(matchType);

            return mapper.Map<ResponseMatchTypeDto>(matchType);
        }
    }
}
