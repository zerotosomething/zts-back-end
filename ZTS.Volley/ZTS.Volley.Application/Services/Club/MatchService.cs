﻿using AutoMapper;
using ZTS.Volley.Application.Dtos.Club.Match;
using ZTS.Volley.Application.Dtos.Sponsors;
using ZTS.Volley.Application.Services.AzureStorage;
using ZTS.Volley.Application.Services.Club.Interfaces;
using ZTS.Volley.Domain.Entities.Club;
using ZTS.Volley.Domain.Entities.Sponsors;
using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Infrastructure.Repositories.Club.Interfaces;
using ZTS.Volley.Infrastructure.Repositories.Staff;

namespace ZTS.Volley.Application.Services.Club
{
    public class MatchService : IMatchService
    {
        private readonly IMatchRepository matchRepository;
        private readonly IMapper mapper;
        private readonly IAzureStorageService storageService;

        public MatchService(IMatchRepository matchRepository, IMapper mapper, IAzureStorageService storageService)
        {
            this.matchRepository = matchRepository;
            this.mapper = mapper;
            this.storageService = storageService;
        }

        public async Task<ResponseMatchDto> Create(MatchDto matchDto)
        {
            var entity = mapper.Map<Match>(matchDto);
            var match = await matchRepository.Create(entity);
            var matchEntity = await matchRepository.GetById(match.Id);

            await storageService.UploadAsync(matchDto.AwayTeamLogo, match.Id);
            await storageService.UploadAsync(matchDto.HomeTeamLogo, match.Id, 1);
            matchEntity.AwayTeamLogo = storageService.StoredImagesUrlAsync(match.Id).Result.First();
            matchEntity.HomeTeamLogo = storageService.StoredImagesUrlAsync(match.Id).Result.Last();
            await matchRepository.Update(matchEntity);

            matchEntity.AwayTeamLogo = storageService.GetImagesUrlAsync(match.Id).Result.First();
            matchEntity.HomeTeamLogo = storageService.GetImagesUrlAsync(match.Id).Result.Last();

            return mapper.Map<ResponseMatchDto>(matchEntity);
        }

        public async Task Delete(Guid id)
        {
            var match = await matchRepository.GetById(id);

            if (match is null)
            {
                throw new KeyNotFoundException();
            }

            try
            {
                var imageURL = await storageService.StoredImagesUrlAsync(id);
                var blobName = imageURL.First().Split('/').Last();
                await storageService.DeleteAsync(blobName);
            }
            catch (Exception)
            {
            }

            await matchRepository.Delete(match);
        }

        public async Task<List<ResponseMatchDto>> GetAll()
        {
            var matches = await matchRepository.GetAll();

            foreach (var match in matches)
            {
                try
                {
                    match.AwayTeamLogo = storageService.GetImagesUrlAsync(match.Id).Result.First();
                    match.HomeTeamLogo = storageService.GetImagesUrlAsync(match.Id).Result.Last();
                }
                catch (Exception)
                {
                }
            }

            return mapper.Map<List<ResponseMatchDto>>(matches);
        }

        public async Task<ResponseMatchDto> GetById(Guid id)
        {
            var match = await matchRepository.GetById(id);

            if (match is null)
            {
                throw new KeyNotFoundException();
            }

            try
            {
                match.AwayTeamLogo = storageService.GetImagesUrlAsync(id).Result.First();
                match.HomeTeamLogo = storageService.GetImagesUrlAsync(id).Result.Last();
            }
            catch (Exception)
            {
            }

            return mapper.Map<ResponseMatchDto>(match);
        }

        public async Task<List<ResponseMatchDto>> GetById(List<Guid> ids)
        {
            var matches = await matchRepository.GetById(ids);

            foreach (var match in matches)
            {
                try
                {
                    match.AwayTeamLogo = storageService.GetImagesUrlAsync(match.Id).Result.First();
                    match.HomeTeamLogo = storageService.GetImagesUrlAsync(match.Id).Result.Last();
                }
                catch (Exception)
                {
                }
            }

            return mapper.Map<List<ResponseMatchDto>>(matches);
        }

        public async Task<ResponseMatchDto> Update(Guid id, MatchDto matchDto)
        {
            var match = await matchRepository.GetById(id);

            if (match is null)
            {
                throw new KeyNotFoundException();
            }

            mapper.Map(matchDto, match);

            if (!match.AwayTeamLogo.Contains("zts.blob"))
            {
                var imageURL = await storageService.StoredImagesUrlAsync(id);
                var blobName = imageURL.First().Split('/').Last();
                await storageService.DeleteAsync(blobName);

                await storageService.UploadAsync(matchDto.AwayTeamLogo, match.Id);
                match.AwayTeamLogo = storageService.StoredImagesUrlAsync(match.Id).Result.First();
                await matchRepository.Update(match);
                match.AwayTeamLogo = storageService.GetImagesUrlAsync(match.Id).Result.First();
            }
            else if (match.AwayTeamLogo.Contains("zts.blob"))
            {
                match.AwayTeamLogo = matchDto.AwayTeamLogo.Split('?').First();
                await matchRepository.Update(match);
            }

            if (!match.HomeTeamLogo.Contains("zts.blob"))
            {
                var imageURL = await storageService.StoredImagesUrlAsync(id);
                var blobName = imageURL.Last().Split('/').Last();
                await storageService.DeleteAsync(blobName);

                await storageService.UploadAsync(matchDto.HomeTeamLogo, match.Id);
                match.HomeTeamLogo = storageService.StoredImagesUrlAsync(match.Id).Result.First();
                await matchRepository.Update(match);
                match.HomeTeamLogo = storageService.GetImagesUrlAsync(match.Id).Result.First();
            }
            else if (match.HomeTeamLogo.Contains("zts.blob"))
            {
                match.HomeTeamLogo = matchDto.HomeTeamLogo.Split('?').First();
                await matchRepository.Update(match);
            }

            return mapper.Map<ResponseMatchDto>(match);
        }

        public async Task<ResponseMatchDto> UpdateScore(Guid id, UpdateMatchScoreDto matchScoreDto)
        {
            var match = await matchRepository.GetById(id);

            if (match is null)
            {
                throw new KeyNotFoundException();
            }

            mapper.Map(matchScoreDto, match);
            await matchRepository.Update(match);

            return mapper.Map<ResponseMatchDto>(match);
        }
    }
}
