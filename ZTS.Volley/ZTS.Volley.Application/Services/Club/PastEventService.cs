﻿using AutoMapper;
using ZTS.Volley.Application.Dtos.Club.PastEvents;
using ZTS.Volley.Application.Services.Club.Interfaces;
using ZTS.Volley.Domain.Entities.Club;
using ZTS.Volley.Infrastructure.Repositories.Club.Interfaces;

namespace ZTS.Volley.Application.Services.Club
{
    public class PastEventService : IPastEventService
    {
        private readonly IPastEventRepository pastEventRepository;
        private readonly IMapper mapper;

        public PastEventService(IPastEventRepository pastEventRepository, IMapper mapper)
        {
            this.pastEventRepository = pastEventRepository;
            this.mapper = mapper;
        }

        public async Task<ResponsePastEventDto> Create(PastEventDto eventDto)
        {
            var entity = mapper.Map<PastEvent>(eventDto);
            var pastEvent = await pastEventRepository.Create(entity);

            return mapper.Map<ResponsePastEventDto>(pastEvent);
        }

        public async Task Delete(Guid id)
        {
            var pastEvent = await pastEventRepository.GetById(id);

            if (pastEvent is null)
            {
                throw new KeyNotFoundException();
            }

            await pastEventRepository.Delete(pastEvent);
        }

        public async Task<List<ResponsePastEventDto>> GetAll()
        {
            var pastEvents = await pastEventRepository.GetAll();

            return mapper.Map<List<ResponsePastEventDto>>(pastEvents);
        }

        public async Task<ResponsePastEventDto> GetById(Guid id)
        {
            var pastEvent = await pastEventRepository.GetById(id);

            if (pastEvent is null)
            {
                throw new KeyNotFoundException();
            }

            return mapper.Map<ResponsePastEventDto>(pastEvent);
        }

        public async Task<ResponsePastEventDto> Update(Guid id, PastEventDto eventDto)
        {
            var pastEvent = await pastEventRepository.GetById(id);

            if (pastEvent is null)
            {
                throw new KeyNotFoundException();
            }

            mapper.Map(eventDto, pastEvent);
            await pastEventRepository.Update(pastEvent);

            return mapper.Map<ResponsePastEventDto>(pastEvent);
        }
    }
}
