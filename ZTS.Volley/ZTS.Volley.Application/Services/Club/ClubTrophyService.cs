﻿using AutoMapper;
using ZTS.Volley.Application.Dtos.Club.ClubTrophy;
using ZTS.Volley.Application.Services.Club.Interfaces;
using ZTS.Volley.Domain.Entities.Club;
using ZTS.Volley.Infrastructure.Repositories.Club.Interfaces;
using ZTS.Volley.Utils.Resources;

namespace ZTS.Volley.Application.Services.Club
{
    public class ClubTrophyService : IClubTrophyService
    {
        private readonly IClubTrophyRepository clubTrophyRepository;
        private readonly IMapper mapper;

        public ClubTrophyService(IClubTrophyRepository clubTrophyRepository, IMapper mapper)
        {
            this.clubTrophyRepository = clubTrophyRepository;
            this.mapper = mapper;
        }

        public async Task<ResponseClubTrophyDto> Create(ClubTrophyDto clubTrophyDto)
        {
            var entity = mapper.Map<ClubTrophy>(clubTrophyDto);
            var clubTrophyCreated = await clubTrophyRepository.Create(entity);
            var clubTrophy = await clubTrophyRepository.GetById(clubTrophyCreated.Id);

            return mapper.Map<ResponseClubTrophyDto>(clubTrophy);
        }

        public async Task Delete(Guid id)
        {
            var clubTrophy = await clubTrophyRepository.GetById(id);

            if (clubTrophy is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(ClubTrophyService)));
            }

            await clubTrophyRepository.Delete(clubTrophy);
        }

        public async Task<List<ResponseClubTrophyDto>> GetAll()
        {
            var clubTrophies = await clubTrophyRepository.GetAll();

            return mapper.Map<List<ResponseClubTrophyDto>>(clubTrophies);
        }

        public async Task<ResponseClubTrophyDto> GetById(Guid id)
        {
            var clubTrophy = await clubTrophyRepository.GetById(id);

            if (clubTrophy is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(ClubTrophyService)));
            }

            return mapper.Map<ResponseClubTrophyDto>(clubTrophy);
        }

        public async Task<List<ResponseClubTrophyDto>> GetById(List<Guid> ids)
        {
            var clubTrophies = await clubTrophyRepository.GetById(ids);

            return mapper.Map<List<ResponseClubTrophyDto>>(clubTrophies);
        }

        public async Task<ResponseClubTrophyDto> Update(Guid id, ClubTrophyDto clubTrophyDto)
        {
            var clubTrophy = await clubTrophyRepository.GetById(id);

            if (clubTrophy is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(ClubTrophyService)));
            }

            mapper.Map(clubTrophyDto, clubTrophy);
            await clubTrophyRepository.Update(clubTrophy);
            var modifiedClubTrophy = await clubTrophyRepository.GetById(clubTrophy.Id);

            return mapper.Map<ResponseClubTrophyDto>(modifiedClubTrophy);
        }
    }
}
