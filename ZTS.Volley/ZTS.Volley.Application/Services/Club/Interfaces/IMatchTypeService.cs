﻿using ZTS.Volley.Application.Dtos.Club;

namespace ZTS.Volley.Application.Services.Club.Interfaces
{
    public interface IMatchTypeService
    {
        Task<ResponseMatchTypeDto> Create(MatchTypeDto matchTypeDto);
        Task<ResponseMatchTypeDto> GetById(int id);
        Task<List<ResponseMatchTypeDto>> GetById(List<int> ids);
        Task<List<ResponseMatchTypeDto>> GetAll();
        Task<ResponseMatchTypeDto> Update(int id, MatchTypeDto matchTypeDto);
        Task Delete(int id);
    }
}
