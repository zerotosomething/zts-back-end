﻿using ZTS.Volley.Application.Dtos.Club.PastEvents;

namespace ZTS.Volley.Application.Services.Club.Interfaces
{
    public interface IPastEventService
    {
        Task<ResponsePastEventDto> Create(PastEventDto eventDto);
        Task<ResponsePastEventDto> GetById(Guid id);
        Task<List<ResponsePastEventDto>> GetAll();
        Task<ResponsePastEventDto> Update(Guid id, PastEventDto eventDto);
        Task Delete(Guid id);
    }
}
 