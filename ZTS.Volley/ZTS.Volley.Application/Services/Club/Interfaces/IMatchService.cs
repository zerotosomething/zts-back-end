﻿using ZTS.Volley.Application.Dtos.Club.Match;

namespace ZTS.Volley.Application.Services.Club.Interfaces
{
    public interface IMatchService
    {
        Task<ResponseMatchDto> Create(MatchDto matchDto);
        Task<ResponseMatchDto> GetById(Guid id);
        Task<List<ResponseMatchDto>> GetById(List<Guid> ids);
        Task<List<ResponseMatchDto>> GetAll();
        Task<ResponseMatchDto> UpdateScore(Guid id, UpdateMatchScoreDto matchScoreDto);
        Task<ResponseMatchDto> Update(Guid id, MatchDto matchDto);
        Task Delete(Guid id);
    }
}
