﻿using ZTS.Volley.Application.Dtos.Club.ClubTrophy;

namespace ZTS.Volley.Application.Services.Club.Interfaces
{
    public interface IClubTrophyService
    {
        Task<ResponseClubTrophyDto> Create(ClubTrophyDto clubTrophyDto);
        Task<ResponseClubTrophyDto> GetById(Guid id);
        Task<List<ResponseClubTrophyDto>> GetById(List<Guid> ids);
        Task<List<ResponseClubTrophyDto>> GetAll();
        Task<ResponseClubTrophyDto> Update(Guid id, ClubTrophyDto clubTrophyDto);
        Task Delete(Guid id);
    }
}
