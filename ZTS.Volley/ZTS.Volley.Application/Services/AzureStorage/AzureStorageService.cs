﻿using Azure;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Text.RegularExpressions;
using ZTS.Volley.Application.Dtos.AzureStorage;

namespace ZTS.Volley.Application.Services.AzureStorage
{
    public class AzureStorageService : IAzureStorageService
    {
        private readonly ILogger<AzureStorageService> logger;

        private readonly string storageConnectionString;
        private readonly string storageContainerName;
        private readonly string sasKey;

        public AzureStorageService(IConfiguration configuration, ILogger<AzureStorageService> logger)
        {
            this.logger = logger;

            storageConnectionString = configuration["BlobConnectionString"];
            storageContainerName = configuration["BlobContainerName"];
            sasKey = configuration["SharedAccessSignature"];
        }

        public async Task<BlobResponseDto> DeleteAsync(string blobFilename)
        {
            BlobContainerClient client = new BlobContainerClient(storageConnectionString, storageContainerName);
            BlobClient file = client.GetBlobClient(blobFilename);

            try
            {
                await file.DeleteAsync();
            }
            catch (RequestFailedException ex)
                when (ex.ErrorCode == BlobErrorCode.BlobNotFound)
            {
                logger.LogError($"File {blobFilename} was not found.");

                return new BlobResponseDto { Error = true, Status = $"File with name {blobFilename} not found." };
            }

            return new BlobResponseDto
            {
                Error = false,
                Status = $"File: {blobFilename} has been successfully deleted."
            };
        }

        public async Task<List<string>> StoredImagesUrlAsync(Guid id)
        {
            BlobContainerClient container = new BlobContainerClient(storageConnectionString, storageContainerName);
            List<string> imagesUrl = new List<string>();

            await foreach (BlobItem file in container.GetBlobsAsync())
            {
                var accountName = container.AccountName;
                var containerName = container.Name;
                var blobName = file.Name;

                if (blobName.Contains(id.ToString()))
                {
                    string uri = container.Uri.Host;
                    string header = "https://";
                    var fullUri = $"{header}{uri}/{containerName}/{blobName}";
                    imagesUrl.Add(fullUri);
                }
            }

            return imagesUrl;
        }

        public async Task<List<string>> GetImagesUrlAsync(Guid id)
        {
            BlobContainerClient container = new BlobContainerClient(storageConnectionString, storageContainerName);
            List<string> imagesUrl = new List<string>();

            await foreach (BlobItem file in container.GetBlobsAsync())
            {
                var accountName = container.AccountName;
                var containerName = container.Name;
                var blobName = file.Name;

                if (blobName.Contains(id.ToString()))
                {
                    string uri = container.Uri.Host;
                    string header = "https://";
                    var fullUri = $"{header}{uri}/{containerName}/{blobName}?{sasKey}";
                    imagesUrl.Add(fullUri);
                }
            }

            return imagesUrl;
        }

        public async Task<BlobResponseDto> UploadAsync(string image, Guid objectId, int counter = 0)
        {
            List<IFormFile> files = new List<IFormFile>();

            try
            {
                if ((image.Length % 4 == 0) && Regex.IsMatch(image, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None))
                {
                    byte[] bytes = Convert.FromBase64String(image);
                    MemoryStream stream = new MemoryStream(bytes, 0, bytes.Length);
                    stream.Write(bytes, 0, bytes.Length);
                    IFormFile file = new FormFile(
                        stream, 0, bytes.Length, storageContainerName, "image" + objectId.ToString() + "_" + counter + ".jpeg");
                    files.Add(file);
                    counter++;
                }
            }
            catch
            {
                throw new Exception();
            }

            BlobResponseDto response = new();
            BlobContainerClient container = new BlobContainerClient(storageConnectionString, storageContainerName);

            foreach (var blob in files)
            {
                try
                {
                    BlobClient client = container.GetBlobClient(blob.FileName);

                    await using (Stream? data = blob.OpenReadStream())
                    {
                        await client.UploadAsync(data, new BlobHttpHeaders { ContentType = "image/jpeg" });
                    }

                    response.Status = $"File {blob.FileName} Uploaded Successfully";
                    response.Error = false;
                    response.Blob.Uri = client.Uri.AbsoluteUri;
                    response.Blob.Name = client.Name;
                }
                catch (RequestFailedException ex)
                   when (ex.ErrorCode == BlobErrorCode.BlobAlreadyExists)
                {
                    logger.LogError(
                        $"File with name {blob.FileName} already exists in container. " +
                        $"Set another name to store the file in the container: '{storageContainerName}.'");

                    response.Status = $"File with name {blob.FileName} already exists. Please use another name to store your file.";
                    response.Error = true;

                    return response;
                }
                catch (RequestFailedException ex)
                {
                    logger.LogError($"Unhandled Exception. ID: {ex.StackTrace} - Message: {ex.Message}");

                    response.Status = $"Unexpected error: {ex.StackTrace}. Check log with StackTrace ID.";
                    response.Error = true;

                    return response;
                }
            }

            return response;
        }

        public async Task<BlobResponseDto> UploadVideoAsync(string base64Video, Guid objectId, int counter = 0)
        {
            BlobContainerClient container = new BlobContainerClient(storageConnectionString, storageContainerName);

            var videoBytes = Convert.FromBase64String(base64Video);

            var fileName = $"image{objectId.ToString()}_{counter}.mp4";

            BlobClient blobClient = container.GetBlobClient(fileName);
            blobClient.Upload(new MemoryStream(videoBytes), true);

            return default;
        }
    }
}
