﻿using ZTS.Volley.Application.Dtos.AzureStorage;

namespace ZTS.Volley.Application.Services.AzureStorage
{
    public interface IAzureStorageService
    {
        Task<BlobResponseDto> UploadAsync(string image, Guid objectId, int counter = 0);
        Task<BlobResponseDto> DeleteAsync(string blobFilename);
        Task<List<string>> StoredImagesUrlAsync(Guid id);
        Task<List<string>> GetImagesUrlAsync(Guid id);
    }
}
