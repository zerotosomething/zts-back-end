﻿using AutoMapper;
using System.Text.RegularExpressions;
using ZTS.Volley.Application.Dtos.NewsArticles;
using ZTS.Volley.Application.Services.AzureStorage;
using ZTS.Volley.Application.Services.Interfaces.NewsArticles;
using ZTS.Volley.Domain.Entities.NewsArticles;
using ZTS.Volley.Infrastructure.Repositories.Interfaces;

namespace ZTS.Volley.Application.Services.NewsArticles
{
    public class NewsService : INewsService
    {
        private readonly INewsRepository newsRepository;
        private readonly IContentRepository contentRepository;
        private readonly IMapper mapper;
        private readonly IAzureStorageService storageService;
        private readonly IContentTypeRepository contentTypeRepository;
        private readonly INewsStatusRepository newsStatusRepository;

        public NewsService(INewsRepository newsRepository, IContentRepository contentRepository, INewsStatusRepository newsStatusRepository, IContentTypeRepository contentTypeRepository, IMapper mapper, IAzureStorageService storageService)
        {
            this.newsRepository = newsRepository;
            this.mapper = mapper;
            this.contentRepository = contentRepository;
            this.storageService = storageService;
            this.contentTypeRepository = contentTypeRepository;
            this.newsStatusRepository = newsStatusRepository;
        }

        public async Task<CreateNewsRequestDto> Create(CreateNewsRequestDto newsDto)
        {
            var entity = mapper.Map<News>(newsDto);
            entity.CreationDate = DateTime.UtcNow;
            var news = await newsRepository.Create(entity);
            var contentType = await contentTypeRepository.GetById(newsDto.ContentTypeId);
            news.NewsStatus = await newsStatusRepository.GetById(newsDto.NewsStatusId);
            var dataToUpload = new List<string>();

            if (newsDto.Content is null || string.IsNullOrWhiteSpace(newsDto.Content.FirstOrDefault()))
            {
                return newsDto;
            }

            if (newsDto.ContentTypeId == 1)
            {
                foreach (var content in newsDto.Content)
                {
                    if (!string.IsNullOrWhiteSpace(content) && (content.Length % 4 == 0) && Regex.IsMatch(content, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None))
                    {
                        dataToUpload.Add(content);
                    }
                }

                for (int i = 0; i < dataToUpload.Count; i++)
                {
                    await storageService.UploadAsync(newsDto.Content[i], news.Id, i);
                }

                var urls = await storageService.StoredImagesUrlAsync(news.Id);

                news.Contents = await contentRepository.CreateForNews(news, urls, newsDto.ContentTypeId, contentType);
            }
            else
            {
                foreach (var content in newsDto.Content)
                {
                    if (!string.IsNullOrWhiteSpace(content) && Regex.IsMatch(content, @"http(?:s ?):\/\/ (?: www\.)?youtu(?:be\.com\/ watch\?v =|\.be\/)([\w\-\_] *)(&(amp;)?‌​[\w\?‌​=]*)?"))
                    {
                        dataToUpload.Add(content);
                    }
                }

                if (dataToUpload != null || dataToUpload.Count > 0)
                {
                    news.Contents = await contentRepository.CreateForNews(news, dataToUpload, newsDto.ContentTypeId, contentType);
                }
            }

            if (contentType.Id == 1)
            {
                newsDto.Content = await storageService.GetImagesUrlAsync(news.Id);
            }
            else if (contentType.Id == 2)
            {
                newsDto.Content = contentRepository.GetAllForNews(news.Id).Result.Select(content => content.Data).ToList();
            }
            news.NewsStatusId = news.NewsStatus.Id;

            return newsDto;
        }

        public async Task Delete(Guid id)
        {
            var news = await newsRepository.GetById(id);

            if (news is null)
            {
                throw new KeyNotFoundException();
            }

            try
            {
                var imageURL = await storageService.StoredImagesUrlAsync(id);
                var blobName = imageURL.First().Split('/').Last();
                await storageService.DeleteAsync(blobName);
            }
            catch (Exception)
            {
            }

            await newsRepository.Delete(news);
        }

        public async Task<List<ResponseNewsDto>> GetAll()
        {
            var news = await newsRepository.GetAll();
            var response = mapper.Map<List<ResponseNewsDto>>(news);

            foreach (var res in response)
            {
                res.ContentTypeId = 1;

                var contents = await contentRepository.GetAllForNews(res.Id);
                var content = contents.FirstOrDefault();

                if (content is not null)
                {
                    if (contents.FirstOrDefault().Data.Contains("zts.blob.core"))
                    {
                        try
                        {
                            res.Content = await storageService.GetImagesUrlAsync(res.Id);
                        }
                        catch (Exception)
                        {
                        }
                    }
                    else
                    {
                        res.Content = contents.Select(content => content.Data).ToList();
                        res.ContentTypeId = 2;
                    }
                }
            }

            return response;
        }

        public async Task<ResponseNewsDto> GetById(Guid id)
        {
            var news = await newsRepository.GetById(id);

            if (news is null)
            {
                throw new KeyNotFoundException();
            }

            var response = mapper.Map<ResponseNewsDto>(news);
            var contents = await contentRepository.GetAllForNews(response.Id);

            response.ContentTypeId = 1;

            if (contents.FirstOrDefault() is not null)
            {
                if (contents.FirstOrDefault().Data.Contains("zts.blob.core"))
                {
                    try
                    {
                        response.Content = await storageService.GetImagesUrlAsync(response.Id);
                    }
                    catch (Exception)
                    {
                    }
                }
                else
                {
                    response.Content = contents.Select(content => content.Data).ToList();
                    response.ContentTypeId = 2;
                }
            }

            return response;
        }

        public async Task<List<ResponseNewsDto>> GetByIds(List<Guid> ids)
        {
            var news = await newsRepository.GetById(ids);

            if (news is null)
            {
                throw new KeyNotFoundException();
            }

            var response = mapper.Map<List<ResponseNewsDto>>(news);

            foreach (var item in response)
            {
                item.ContentTypeId = 1;

                var contents = await contentRepository.GetAllForNews(item.Id);
                var content = contents.FirstOrDefault();

                if (content is not null)
                {
                    if (contents.FirstOrDefault().Data.Contains("zts.blob.core"))
                    {
                        try
                        {
                            item.Content = await storageService.GetImagesUrlAsync(item.Id);
                        }
                        catch (Exception)
                        {
                        }
                    }
                    else
                    {
                        item.Content = contents.Select(content => content.Data).ToList();
                        item.ContentTypeId = 2;
                    }
                }
            }

            return response;
        }

        public async Task<ResponseNewsDto> Update(Guid id, CreateNewsRequestDto newsDto)
        {
            var news = await newsRepository.GetById(id);

            if (news is null)
            {
                throw new KeyNotFoundException();
            }

            mapper.Map(newsDto, news);

            var contentType = await contentTypeRepository.GetById(newsDto.ContentTypeId);
            news.NewsStatus = await newsStatusRepository.GetById(newsDto.NewsStatusId);
            var dataToUpload = new List<string>();


            if (newsDto.Content is not null && !string.IsNullOrEmpty(newsDto.Content.FirstOrDefault()))
            {
                try
                {
                    var imagesURL = await storageService.StoredImagesUrlAsync(id);

                    foreach (var image in imagesURL)
                    {
                        var blobName = image.Split('/').Last();
                        await storageService.DeleteAsync(blobName);
                    }
                }
                catch (Exception)
                {
                }
            }

            if (newsDto.Content is null || string.IsNullOrWhiteSpace(newsDto.Content.FirstOrDefault()))
            {
                return mapper.Map<ResponseNewsDto>(news);
            }

            if (newsDto.ContentTypeId == 1)
            {
                foreach (var content in newsDto.Content)
                {
                    if (!string.IsNullOrWhiteSpace(content) && (content.Length % 4 == 0) && Regex.IsMatch(content, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None))
                    {
                        dataToUpload.Add(content);
                    }
                }

                for (int i = 0; i < dataToUpload.Count; i++)
                {
                    await storageService.UploadAsync(newsDto.Content[i], news.Id, i);
                }

                var urls = await storageService.StoredImagesUrlAsync(news.Id);

                news.Contents = await contentRepository.CreateForNews(news, urls, newsDto.ContentTypeId, contentType);
            }
            else
            {
                foreach (var content in newsDto.Content)
                {
                    if (!string.IsNullOrWhiteSpace(content) && Regex.IsMatch(content, @"http(?:s ?):\/\/ (?: www\.)?youtu(?:be\.com\/ watch\?v =|\.be\/)([\w\-\_] *)(&(amp;)?‌​[\w\?‌​=]*)?"))
                    {
                        dataToUpload.Add(content);
                    }
                }

                if (dataToUpload != null || dataToUpload.Count > 0)
                {
                    news.Contents = await contentRepository.CreateForNews(news, dataToUpload, newsDto.ContentTypeId, contentType);
                }
            }

            await newsRepository.Update(news);

            var nws = mapper.Map<ResponseNewsDto>(news);

            if (contentType.Id == 1)
            {
                nws.Content = await storageService.GetImagesUrlAsync(news.Id);
                nws.ContentTypeId = 1;
            }
            else if (contentType.Id == 2)
            {
                nws.Content = contentRepository.GetAllForNews(news.Id).Result.Select(content => content.Data).ToList();
                nws.ContentTypeId = 2;
            }

            return nws;
        }
    }
}
