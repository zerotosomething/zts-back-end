﻿using AutoMapper;
using ZTS.Volley.Application.Dtos.NewsArticles;
using ZTS.Volley.Application.Services.Interfaces.NewsArticles;
using ZTS.Volley.Domain.Entities.NewsArticles;
using ZTS.Volley.Infrastructure.Repositories.Interfaces;

namespace ZTS.Volley.Application.Services.NewsArticles
{
    public class ContentTypeService : IContentTypeService
    {
        private readonly IContentTypeRepository contentTypeRepository;
        private readonly IMapper mapper;

        public ContentTypeService(IContentTypeRepository contentTypeRepository,
            IMapper mapper)
        {
            this.contentTypeRepository = contentTypeRepository;
            this.mapper = mapper;
        }

        public async Task<ResponseContentTypeDto> Create(ContentTypeDto contentTypeDto)
        {
            var entity = mapper.Map<ContentType>(contentTypeDto);
            var contentType = await contentTypeRepository.Create(entity);

            return mapper.Map<ResponseContentTypeDto>(contentType);
        }

        public async Task Delete(int id)
        {
            var contentType = await contentTypeRepository.GetById(id);

            if (contentType is null)
            {
                throw new KeyNotFoundException();
            }

            await contentTypeRepository.Delete(contentType);
        }

        public async Task<List<ResponseContentTypeDto>> GetAll()
        {
            var content = await contentTypeRepository.GetAll();

            return mapper.Map<List<ResponseContentTypeDto>>(content);
        }

        public async Task<ResponseContentTypeDto> GetById(int id)
        {
            var contentType = await contentTypeRepository.GetById(id);

            if (contentType is null)
            {
                throw new KeyNotFoundException();
            }

            return mapper.Map<ResponseContentTypeDto>(contentType);
        }

        public async Task<List<ResponseContentTypeDto>> GetById(List<int> ids)
        {
            var contentType = await contentTypeRepository.GetById(ids);

            return mapper.Map<List<ResponseContentTypeDto>>(contentType);
        }

        public async Task<ResponseContentTypeDto> Update(int id, ContentTypeDto staffMemberDto)
        {
            var contentType = await contentTypeRepository.GetById(id);

            if (contentType is null)
            {
                throw new KeyNotFoundException();
            }

            mapper.Map(staffMemberDto, contentType);
            await contentTypeRepository.Update(contentType);

            return mapper.Map<ResponseContentTypeDto>(contentType);
        }
    }
}
