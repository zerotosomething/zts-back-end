﻿using AutoMapper;
using Microsoft.Extensions.Azure;
using ZTS.Volley.Application.Dtos.NewsArticles;
using ZTS.Volley.Application.Services.AzureStorage;
using ZTS.Volley.Application.Services.Interfaces.NewsArticles;
using ZTS.Volley.Domain.Entities.NewsArticles;
using ZTS.Volley.Infrastructure.Repositories.Interfaces;

namespace ZTS.Volley.Application.Services.NewsArticles
{
    public class ContentService : IContentService
    {
        private readonly IContentRepository contentRepository;
        private readonly IMapper mapper;
        private readonly IAzureStorageService storageService;

        public ContentService(IContentRepository contentRepository,
            IMapper mapper, IAzureStorageService storageService)
        {
            this.contentRepository = contentRepository;
            this.mapper = mapper;
            this.storageService = storageService;
        }

        public async Task<ResponseContentDto> Create(ContentDto contentDto)
        {
            var entity = mapper.Map<Content>(contentDto);
            var content = await contentRepository.Create(entity);

            //if (content.ContentTypeId == 1 && string.IsNullOrWhiteSpace(content.Data))
            //{
            //    var picture = storageService.UploadAsync(content.Data, content.Id);
            //    content.Data = storageService.StoredImageUrlAsync(content.Id).Result;
            //}


            return mapper.Map<ResponseContentDto>(content);
        }

        public async Task Delete(Guid id)
        {
            var content = await contentRepository.GetById(id);

            if (content is null)
            {
                throw new KeyNotFoundException();
            }

            await contentRepository.Delete(content);
        }

        public async Task<List<ResponseContentDto>> GetAll()
        {
            var content = await contentRepository.GetAll();

            return mapper.Map<List<ResponseContentDto>>(content);
        }

        public async Task<ResponseContentDto> GetById(Guid id)
        {
            var content = await contentRepository.GetById(id);

            if (content is null)
            {
                throw new KeyNotFoundException();
            }

            return mapper.Map<ResponseContentDto>(content);
        }

        public async Task<List<ResponseContentDto>> GetById(List<Guid> ids)
        {
            var content = await contentRepository.GetById(ids);

            return mapper.Map<List<ResponseContentDto>>(content);
        }

        public async Task<ResponseContentDto> Update(Guid id, ContentDto staffMemberDto)
        {
            var content = await contentRepository.GetById(id);

            if (content is null)
            {
                throw new KeyNotFoundException();
            }

            mapper.Map(staffMemberDto, content);
            await contentRepository.Update(content);

            return mapper.Map<ResponseContentDto>(content);
        }
    }
}
