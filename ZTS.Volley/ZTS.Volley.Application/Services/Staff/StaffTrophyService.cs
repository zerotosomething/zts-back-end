﻿using AutoMapper;
using ZTS.Volley.Application.Dtos.Staff.StaffTrophy;
using ZTS.Volley.Application.Services.Staff.Interfaces;
using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Domain.Entities.Staff.JoiningEntities;
using ZTS.Volley.Infrastructure.Repositories.Staff.Interfaces;
using ZTS.Volley.Utils.Resources;

namespace ZTS.Volley.Application.Services.Staff
{
    public class StaffTrophyService : IStaffTrophyService
    {
        private readonly IStaffTrophyRepository staffTrophyRepository;
        private readonly IStaffPositionHistoryRepository staffPositionHistoryRepository;
        private readonly IMapper mapper;

        public StaffTrophyService(IStaffTrophyRepository staffTrophyRepository,
            IStaffPositionHistoryRepository staffPositionHistoryRepository,
            IMapper mapper)
        {
            this.staffTrophyRepository = staffTrophyRepository;
            this.staffPositionHistoryRepository = staffPositionHistoryRepository;
            this.mapper = mapper;
        }

        public async Task AddTrophyToStaff(AddTrophyToStaffDto addTrophyToStaffDto)
        {
            var positionTrophy = mapper.Map<StaffTrophyPositionHistory>(addTrophyToStaffDto);
            var staffPosition = await staffPositionHistoryRepository.GetById(addTrophyToStaffDto.StaffPositionId);

            if (staffPosition is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(addTrophyToStaffDto.StaffPositionId, nameof(StaffTrophyService)));
            }

            staffPosition.StaffTrophyPositionHistories.Add(positionTrophy);
            await staffPositionHistoryRepository.Update(staffPosition);
        }

        public async Task<ResponseStaffTrophyDto> Create(StaffTrophyDto staffTrophyDto)
        {
            var entity = mapper.Map<StaffTrophy>(staffTrophyDto);
            var staffTrophy = await staffTrophyRepository.Create(entity);

            return mapper.Map<ResponseStaffTrophyDto>(staffTrophy);
        }

        public async Task Delete(Guid id)
        {
            var staffTrophy = await staffTrophyRepository.GetById(id);

            if (staffTrophy is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(StaffTrophyService)));
            }

            await staffTrophyRepository.Delete(staffTrophy);
        }

        public async Task Delete(Guid staffPositionHistoryId, Guid trophyId)
        {
            var staffPosition = await staffPositionHistoryRepository.GetById(staffPositionHistoryId);
            var trophy = await staffTrophyRepository.GetById(trophyId);

            if (staffPosition == null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(staffPositionHistoryId, nameof(StaffTrophyService)));
            }

            if (trophy == null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(trophyId, nameof(StaffTrophyService)));
            }

            await staffTrophyRepository.RemoveStaffTrophy(staffPositionHistoryId, trophyId);
        }

        public async Task<List<ResponseStaffTrophyDto>> GetAll()
        {
            var staffTrophies = await staffTrophyRepository.GetAll();

            return mapper.Map<List<ResponseStaffTrophyDto>>(staffTrophies);
        }

        public async Task<ResponseStaffTrophyDto> GetById(Guid id)
        {
            var staffTrophy = await staffTrophyRepository.GetById(id);

            if (staffTrophy is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(StaffTrophyService)));
            }

            return mapper.Map<ResponseStaffTrophyDto>(staffTrophy);
        }

        public async Task<List<ResponseStaffTrophyDto>> GetById(List<Guid> ids)
        {
            var staffTrophies = await staffTrophyRepository.GetById(ids);

            return mapper.Map<List<ResponseStaffTrophyDto>>(staffTrophies);
        }

        public async Task<ResponseStaffTrophyDto> Update(Guid id, StaffTrophyDto staffTrophyDto)
        {
            var staffTrophy = await staffTrophyRepository.GetById(id);

            if (staffTrophy is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(StaffTrophyService)));
            }

            mapper.Map(staffTrophyDto, staffTrophy);
            await staffTrophyRepository.Update(staffTrophy);

            return mapper.Map<ResponseStaffTrophyDto>(staffTrophy);
        }
    }
}
