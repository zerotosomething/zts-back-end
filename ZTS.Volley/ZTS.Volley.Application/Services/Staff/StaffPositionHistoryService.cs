﻿using AutoMapper;
using ZTS.Volley.Application.Dtos.Staff.StaffPositionHistory;
using ZTS.Volley.Application.Services.Staff.Interfaces;
using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Infrastructure.Repositories.Staff.Interfaces;
using ZTS.Volley.Utils.Resources;

namespace ZTS.Volley.Application.Services.Staff
{
    public class StaffPositionHistoryService : IStaffPositionHistoryService
    {
        private readonly IStaffPositionHistoryRepository staffPositionHistoryRepository;
        private readonly IMapper mapper;

        public StaffPositionHistoryService(IStaffPositionHistoryRepository staffPositionHistoryRepository,
            IMapper mapper)
        {
            this.staffPositionHistoryRepository = staffPositionHistoryRepository;
            this.mapper = mapper;
        }

        public async Task<ResponseStaffPositionHistoryDto> Create(StaffPositionHistoryDto staffPositionHistoryDto)
        {
            var entity = mapper.Map<StaffPositionHistory>(staffPositionHistoryDto);
            var staffPositionHistory = await staffPositionHistoryRepository.Create(entity);

            return mapper.Map<ResponseStaffPositionHistoryDto>(staffPositionHistory);
        }

        public async Task<ResponseStaffPositionHistoryDto> Create(Guid staffMemberId, int positionId, DateTime startTime)
        {
            var entityDto = new StaffPositionHistoryDto
            {
                StaffMemberId = staffMemberId,
                PositionId = positionId,
                StartTime = startTime,
                EndTime = null
            };

            var entity = mapper.Map<StaffPositionHistory>(entityDto);
            var staffPositionHistory = await staffPositionHistoryRepository.Create(entity);
            var createdStaffPositionHistory = await staffPositionHistoryRepository.GetById(staffPositionHistory.Id);

            return mapper.Map<ResponseStaffPositionHistoryDto>(createdStaffPositionHistory);
        }

        public async Task Delete(Guid id)
        {
            var staffPositionHistory = await staffPositionHistoryRepository.GetById(id);

            if (staffPositionHistory is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(StaffPositionHistory)));
            }

            await staffPositionHistoryRepository.Delete(staffPositionHistory);
        }

        public async Task<List<ResponseStaffPositionHistoryDto>> GetAll()
        {
            var staffPositionHistories = await staffPositionHistoryRepository.GetAll();

            return mapper.Map<List<ResponseStaffPositionHistoryDto>>(staffPositionHistories);
        }

        public async Task<ResponseStaffPositionHistoryDto> GetById(Guid id)
        {
            var staffPositionHistory = await staffPositionHistoryRepository.GetById(id);

            if (staffPositionHistory is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(StaffPositionHistory)));
            }

            return mapper.Map<ResponseStaffPositionHistoryDto>(staffPositionHistory);
        }

        public async Task<List<ResponseStaffPositionHistoryDto>> GetById(List<Guid> ids)
        {
            var staffPositionHistories = await staffPositionHistoryRepository.GetById(ids);

            return mapper.Map<List<ResponseStaffPositionHistoryDto>>(staffPositionHistories);
        }

        public async Task<ResponseStaffPositionHistoryDto> Update(Guid id, StaffPositionHistoryDto staffPositionHistoryDto)
        {
            var staffPositionHistory = await staffPositionHistoryRepository.GetById(id);

            if (staffPositionHistory is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(StaffPositionHistory)));
            }

            mapper.Map(staffPositionHistoryDto, staffPositionHistory);
            await staffPositionHistoryRepository.Update(staffPositionHistory);

            return mapper.Map<ResponseStaffPositionHistoryDto>(staffPositionHistory);
        }
    }
}
