﻿using AutoMapper;
using System.Text.RegularExpressions;
using ZTS.Volley.Application.Dtos.Staff.StaffMember;
using ZTS.Volley.Application.Dtos.Staff.StaffPositionHistory;
using ZTS.Volley.Application.Services.AzureStorage;
using ZTS.Volley.Application.Services.Staff.Interfaces;
using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Infrastructure.Repositories.Staff.Interfaces;
using ZTS.Volley.Utils.Resources;

namespace ZTS.Volley.Application.Services.Staff
{
    public class StaffMemberService : IStaffMemberService
    {
        private readonly IStaffMemberRepository staffMemberRepository;
        private readonly IStaffPositionHistoryService staffPositionHistoryService;
        private readonly IPositionService positionService;
        private readonly IMapper mapper;
        private readonly IAzureStorageService storageService;

        public StaffMemberService(IStaffMemberRepository staffMemberRepository,
            IStaffPositionHistoryService staffPositionHistoryService,
            IMapper mapper,
            IPositionService positionService,
            IAzureStorageService storageService)
        {
            this.staffMemberRepository = staffMemberRepository;
            this.staffPositionHistoryService = staffPositionHistoryService;
            this.mapper = mapper;
            this.positionService = positionService;
            this.storageService = storageService;
        }

        public async Task<ResponseStaffMemberDto> Create(StaffMemberDto staffMemberDto)
        {
            var uploadToCloud = false;

            ValidateStaffTypePosition(staffMemberDto);
            await positionService.GetById(staffMemberDto.Position.Id);
            var entity = mapper.Map<StaffMember>(staffMemberDto);
            var photo = entity.ProfileImage;

            if (!string.IsNullOrWhiteSpace(photo) && (photo.Length % 4 == 0) && Regex.IsMatch(photo, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None))
            {
                entity.ProfileImage = string.Empty;
                uploadToCloud = true;
            }

            var staffMember = await staffMemberRepository.Create(entity);

            if (uploadToCloud)
            {
                await storageService.UploadAsync(photo, staffMember.Id);
                staffMember.ProfileImage = storageService.StoredImagesUrlAsync(staffMember.Id).Result.First();
            }

            await staffMemberRepository.Update(staffMember);
            await staffPositionHistoryService.Create(staffMember.Id,
                                                    staffMemberDto.Position.Id,
                                                    staffMemberDto.Position.StartTime.ToUniversalTime());
            var createdStaffMember = await staffMemberRepository.GetById(staffMember.Id);

            if (uploadToCloud)
            {
                createdStaffMember.ProfileImage = storageService.GetImagesUrlAsync(staffMember.Id).Result.First();
            }

            return mapper.Map<ResponseStaffMemberDto>(createdStaffMember);
        }

        public async Task Delete(Guid id)
        {
            var staffMember = await staffMemberRepository.GetById(id);

            if (staffMember is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(StaffMemberService)));
            }

            try
            {
                var imageURL = await storageService.StoredImagesUrlAsync(id);
                var blobName = imageURL.First().Split('/').Last();
                await storageService.DeleteAsync(blobName);
            }
            catch (Exception)
            {
            }

            await staffMemberRepository.Delete(staffMember);
        }

        public async Task<List<ResponseStaffMemberDto>> GetAll()
        {
            var staffMembers = await staffMemberRepository.GetAll();

            foreach (var member in staffMembers)
            {
                try
                {
                    member.ProfileImage = storageService.GetImagesUrlAsync(member.Id).Result.First();
                }
                catch (Exception)
                {
                }
            }

            return mapper.Map<List<ResponseStaffMemberDto>>(staffMembers);
        }

        public async Task<List<ResponseStaffMemberDto>> GetByName(string name)
        {
            var staffMembers = await staffMemberRepository.GetByName(name);

            foreach (var member in staffMembers)
            {
                member.ProfileImage = storageService.GetImagesUrlAsync(member.Id).Result.First();
            }

            return mapper.Map<List<ResponseStaffMemberDto>>(staffMembers);
        }

        public async Task<ResponseStaffMemberDto> GetById(Guid id)
        {
            var staffMember = await staffMemberRepository.GetById(id);

            if (staffMember is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(StaffMemberService)));
            }

            try
            {
                staffMember.ProfileImage = storageService.GetImagesUrlAsync(id).Result.First();
            }
            catch (Exception)
            {
            }

            return mapper.Map<ResponseStaffMemberDto>(staffMember);
        }

        public async Task<List<ResponseStaffMemberDto>> GetById(List<Guid> ids)
        {
            var staffMembers = await staffMemberRepository.GetById(ids);

            foreach (var member in staffMembers)
            {
                try
                {
                    member.ProfileImage = storageService.GetImagesUrlAsync(member.Id).Result.First();
                }
                catch (Exception)
                {
                }
            }

            return mapper.Map<List<ResponseStaffMemberDto>>(staffMembers);
        }

        public async Task<ResponseStaffMemberDto> Update(Guid id, StaffMemberDto staffMemberDto)
        {
            var staffMember = await staffMemberRepository.GetById(id);

            if (staffMember is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(StaffMemberService)));
            }

            ValidateStaffTypePosition(staffMemberDto);
            mapper.Map(staffMemberDto, staffMember);
            var oldPosition = staffMember.StaffPositionHistories.First(position => position.EndTime == default);

            if (staffMemberDto.Position.Id != oldPosition.PositionId)
            {
                var newPosition = await staffPositionHistoryService.Create(id, staffMemberDto.Position.Id, staffMemberDto.Position.StartTime.ToUniversalTime());
                oldPosition.EndTime = newPosition.StartTime;
                var oldPositionDto = mapper.Map<StaffPositionHistoryDto>(oldPosition);
                await staffPositionHistoryService.Update(oldPosition.Id, oldPositionDto);
            }

            await staffMemberRepository.Update(staffMember);
            var modifiedStaffMember = await staffMemberRepository.GetById(id);

            var imageURL = await storageService.StoredImagesUrlAsync(id);

            if (string.IsNullOrWhiteSpace(imageURL.FirstOrDefault()) && !string.IsNullOrWhiteSpace(staffMemberDto.ProfileImage) && (staffMemberDto.ProfileImage.Length % 4 == 0) && Regex.IsMatch(staffMemberDto.ProfileImage, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None))
            {
                await storageService.UploadAsync(staffMemberDto.ProfileImage, staffMember.Id);
                modifiedStaffMember.ProfileImage = storageService.StoredImagesUrlAsync(modifiedStaffMember.Id).Result.First();
                await staffMemberRepository.Update(staffMember);
                modifiedStaffMember.ProfileImage = storageService.GetImagesUrlAsync(modifiedStaffMember.Id).Result.First();
            }
            else if(!string.IsNullOrWhiteSpace(imageURL.FirstOrDefault()) && !string.IsNullOrWhiteSpace(staffMemberDto.ProfileImage) && (staffMemberDto.ProfileImage.Length % 4 == 0) && Regex.IsMatch(staffMemberDto.ProfileImage, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None))
            {
                var blobName = imageURL.First().Split('/').Last();
                await storageService.DeleteAsync(blobName);

                await storageService.UploadAsync(staffMemberDto.ProfileImage, staffMember.Id);
                modifiedStaffMember.ProfileImage = storageService.StoredImagesUrlAsync(modifiedStaffMember.Id).Result.First();
                await staffMemberRepository.Update(staffMember);
                modifiedStaffMember.ProfileImage = storageService.GetImagesUrlAsync(modifiedStaffMember.Id).Result.First();
            }
            else if (!string.IsNullOrWhiteSpace(imageURL.FirstOrDefault()) && string.IsNullOrWhiteSpace(staffMemberDto.ProfileImage))
            {
                var blobName = imageURL.First().Split('/').Last();
                await storageService.DeleteAsync(blobName);
                modifiedStaffMember.ProfileImage = string.Empty;
                await staffMemberRepository.Update(staffMember);
            }

            return mapper.Map<ResponseStaffMemberDto>(modifiedStaffMember);
        }

        public async Task<StaffHistoryDto> GetPositionHistory(Guid id)
        {
            var staffMember = await staffMemberRepository.GetById(id);

            if (staffMember is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(StaffMemberService)));
            }

            return mapper.Map<StaffHistoryDto>(staffMember);
        }

        public async Task<StaffHistoryDto> DeleteLastPosition(Guid id)
        {
            var staffMember = await staffMemberRepository.GetById(id);

            if (staffMember is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(StaffMemberService)));
            }

            if (staffMember.StaffPositionHistories.Count == 1)
            {
                throw new Exception("This operation can't be done, there is only one position left");
            }

            var oldCurrentPosition = staffMember.StaffPositionHistories.Where(positionHistory => positionHistory.EndTime == default).First();
            var newCurrentPosition = staffMember.StaffPositionHistories.Where(positionHistory => positionHistory.EndTime == oldCurrentPosition.StartTime).First();
            newCurrentPosition.EndTime = default;
            await staffPositionHistoryService.Delete(oldCurrentPosition.Id);
            staffMember.StaffPositionHistories.Remove(oldCurrentPosition);

            return mapper.Map<StaffHistoryDto>(staffMember);
        }

        private void ValidateStaffTypePosition(StaffMemberDto staffMemberDto)
        {
            // StaffTypeId = 2 - coach
            if (staffMemberDto.StaffTypeId == 2)
            {
                staffMemberDto.Position.Id = 7;
            }
        }
    }
}
