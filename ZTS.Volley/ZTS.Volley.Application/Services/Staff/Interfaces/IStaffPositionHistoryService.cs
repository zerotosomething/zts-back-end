﻿using ZTS.Volley.Application.Dtos.Staff.StaffPositionHistory;

namespace ZTS.Volley.Application.Services.Staff.Interfaces
{
    public interface IStaffPositionHistoryService
    {
        Task<ResponseStaffPositionHistoryDto> Create(StaffPositionHistoryDto staffPositionHistoryDto);
        Task<ResponseStaffPositionHistoryDto> Create(Guid staffMemberId, int positionId, DateTime startTime);
        Task<ResponseStaffPositionHistoryDto> GetById(Guid id);
        Task<List<ResponseStaffPositionHistoryDto>> GetById(List<Guid> ids);
        Task<List<ResponseStaffPositionHistoryDto>> GetAll();
        Task<ResponseStaffPositionHistoryDto> Update(Guid id, StaffPositionHistoryDto staffPositionHistoryDto);
        Task Delete(Guid id);
    }
}
