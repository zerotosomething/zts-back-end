﻿using ZTS.Volley.Application.Dtos.Staff.Position;

namespace ZTS.Volley.Application.Services.Staff.Interfaces
{
    public interface IPositionService
    {
        Task<ResponsePositionDto> Create(PositionDto positionDto);
        Task<ResponsePositionDto> GetById(int id);
        Task<List<ResponsePositionDto>> GetById(List<int> ids);
        Task<List<ResponsePositionDto>> GetAll();
        Task<ResponsePositionDto> Update(int id, PositionDto positionDto);
        Task Delete(int id);
    }
}
