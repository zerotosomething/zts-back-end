﻿using ZTS.Volley.Application.Dtos.Staff.StaffTrophy;

namespace ZTS.Volley.Application.Services.Staff.Interfaces
{
    public interface IStaffTrophyService
    {
        Task<ResponseStaffTrophyDto> Create(StaffTrophyDto staffTrophyDto);
        Task AddTrophyToStaff(AddTrophyToStaffDto addTrophyToStaffDto);
        Task<ResponseStaffTrophyDto> GetById(Guid id);
        Task<List<ResponseStaffTrophyDto>> GetById(List<Guid> ids);
        Task<List<ResponseStaffTrophyDto>> GetAll();
        Task<ResponseStaffTrophyDto> Update(Guid id, StaffTrophyDto staffTrophyDto);
        Task Delete(Guid id);
        Task Delete(Guid staffPositionHistoryId, Guid trophyId);
    }
}
