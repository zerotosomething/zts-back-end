﻿using ZTS.Volley.Application.Dtos.Staff.StaffType;

namespace ZTS.Volley.Application.Services.Staff.Interfaces
{
    public interface IStaffTypeService
    {
        Task<ResponseStaffTypeDto> Create(StaffTypeDto staffTypeDto);
        Task<ResponseStaffTypeDto> GetById(int id);
        Task<List<ResponseStaffTypeDto>> GetById(List<int> ids);
        Task<List<ResponseStaffTypeDto>> GetAll();
        Task<ResponseStaffTypeDto> Update(int id, StaffTypeDto staffTypeDto);
        Task Delete(int id);
    }
}
