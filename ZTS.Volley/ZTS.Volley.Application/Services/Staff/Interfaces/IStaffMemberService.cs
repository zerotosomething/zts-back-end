﻿using ZTS.Volley.Application.Dtos.Staff.StaffMember;

namespace ZTS.Volley.Application.Services.Staff.Interfaces
{
    public interface IStaffMemberService
    {
        Task<ResponseStaffMemberDto> Create(StaffMemberDto staffMemberDto);
        Task<ResponseStaffMemberDto> GetById(Guid id);
        Task<List<ResponseStaffMemberDto>> GetById(List<Guid> ids);
        Task<List<ResponseStaffMemberDto>> GetAll();
        Task<List<ResponseStaffMemberDto>> GetByName(string name);
        Task<StaffHistoryDto> GetPositionHistory(Guid id);
        Task<ResponseStaffMemberDto> Update(Guid id, StaffMemberDto staffMemberDto);
        Task Delete(Guid id);
        Task<StaffHistoryDto> DeleteLastPosition(Guid id);
    }
}
