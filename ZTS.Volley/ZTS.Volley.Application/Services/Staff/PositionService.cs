﻿using AutoMapper;
using ZTS.Volley.Application.Dtos.Staff.Position;
using ZTS.Volley.Application.Services.Staff.Interfaces;
using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Infrastructure.Repositories.Staff.Interfaces;
using ZTS.Volley.Utils.Resources;

namespace ZTS.Volley.Application.Services.Staff
{
    public class PositionService : IPositionService
    {
        private readonly IPositionRepository positionRepository;
        private readonly IMapper mapper;

        public PositionService(IPositionRepository positionRepository,
            IMapper mapper)
        {
            this.positionRepository = positionRepository;
            this.mapper = mapper;
        }

        public async Task<ResponsePositionDto> Create(PositionDto positionDto)
        {
            var entity = mapper.Map<Position>(positionDto);
            var position = await positionRepository.Create(entity);

            return mapper.Map<ResponsePositionDto>(position);
        }

        public async Task Delete(int id)
        {
            var position = await positionRepository.GetById(id);

            if (position is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(PositionService)));
            }

            await positionRepository.Delete(position);
        }

        public async Task<List<ResponsePositionDto>> GetAll()
        {
            var positions = await positionRepository.GetAll();

            return mapper.Map<List<ResponsePositionDto>>(positions);
        }

        public async Task<ResponsePositionDto> GetById(int id)
        {
            var position = await positionRepository.GetById(id);

            if (position is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(PositionService)));
            }

            return mapper.Map<ResponsePositionDto>(position);
        }

        public async Task<List<ResponsePositionDto>> GetById(List<int> ids)
        {
            var positions = await positionRepository.GetById(ids);

            return mapper.Map<List<ResponsePositionDto>>(positions);
        }

        public async Task<ResponsePositionDto> Update(int id, PositionDto positionDto)
        {
            var position = await positionRepository.GetById(id);

            if (position is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(PositionService)));
            }

            mapper.Map(positionDto, position);
            await positionRepository.Update(position);

            return mapper.Map<ResponsePositionDto>(position);
        }
    }
}
