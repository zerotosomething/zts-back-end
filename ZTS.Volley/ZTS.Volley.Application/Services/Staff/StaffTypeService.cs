﻿using AutoMapper;
using ZTS.Volley.Application.Dtos.Staff.StaffType;
using ZTS.Volley.Application.Services.Staff.Interfaces;
using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Infrastructure.Repositories.Staff.Interfaces;
using ZTS.Volley.Utils.Resources;

namespace ZTS.Volley.Application.Services.Staff
{
    public class StaffTypeService : IStaffTypeService
    {
        private readonly IStaffTypeRepository staffTypeRepository;
        private readonly IMapper mapper;

        public StaffTypeService(IStaffTypeRepository staffTypeRepository,
            IMapper mapper)
        {
            this.staffTypeRepository = staffTypeRepository;
            this.mapper = mapper;
        }

        public async Task<ResponseStaffTypeDto> Create(StaffTypeDto staffTypeDto)
        {
            var entity = mapper.Map<StaffType>(staffTypeDto);
            var staffType = await staffTypeRepository.Create(entity);

            return mapper.Map<ResponseStaffTypeDto>(staffType);
        }

        public async Task Delete(int id)
        {
            var staffType = await staffTypeRepository.GetById(id);

            if (staffType is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(StaffType)));
            }

            await staffTypeRepository.Delete(staffType);
        }

        public async Task<List<ResponseStaffTypeDto>> GetAll()
        {
            var staffTypes = await staffTypeRepository.GetAll();

            return mapper.Map<List<ResponseStaffTypeDto>>(staffTypes);
        }

        public async Task<ResponseStaffTypeDto> GetById(int id)
        {
            var staffType = await staffTypeRepository.GetById(id);

            if (staffType is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(StaffType)));
            }

            return mapper.Map<ResponseStaffTypeDto>(staffType);
        }

        public async Task<List<ResponseStaffTypeDto>> GetById(List<int> ids)
        {
            var staffTypes = await staffTypeRepository.GetById(ids);

            return mapper.Map<List<ResponseStaffTypeDto>>(staffTypes);
        }

        public async Task<ResponseStaffTypeDto> Update(int id, StaffTypeDto staffTypeDto)
        {
            var staffType = await staffTypeRepository.GetById(id);

            if (staffType is null)
            {
                throw new KeyNotFoundException(ErrorMessages.GetServiceKeyNotFound(id, nameof(StaffType)));
            }

            mapper.Map(staffTypeDto, staffType);
            await staffTypeRepository.Update(staffType);

            return mapper.Map<ResponseStaffTypeDto>(staffType);
        }
    }
}
