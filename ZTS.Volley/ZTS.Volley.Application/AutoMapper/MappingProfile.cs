﻿using AutoMapper;
using ZTS.Volley.Application.Dtos;
using ZTS.Volley.Application.Dtos.Club;
using ZTS.Volley.Application.Dtos.Club.ClubTrophy;
using ZTS.Volley.Application.Dtos.Club.Match;
using ZTS.Volley.Application.Dtos.Club.PastEvents;
using ZTS.Volley.Application.Dtos.NewsArticles;
using ZTS.Volley.Application.Dtos.Sponsors;
using ZTS.Volley.Application.Dtos.Staff.Position;
using ZTS.Volley.Application.Dtos.Staff.StaffMember;
using ZTS.Volley.Application.Dtos.Staff.StaffPositionHistory;
using ZTS.Volley.Application.Dtos.Staff.StaffTrophy;
using ZTS.Volley.Application.Dtos.Staff.StaffType;
using ZTS.Volley.Domain.Entities;
using ZTS.Volley.Domain.Entities.Club;
using ZTS.Volley.Domain.Entities.NewsArticles;
using ZTS.Volley.Domain.Entities.Sponsors;
using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Domain.Entities.Staff.JoiningEntities;
using MatchType = ZTS.Volley.Domain.Entities.Club.MatchType;

namespace ZTS.Volley.Application.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            ConfigureAgeCategoryMapping();
            ConfigureStaffTypeMapping();
            ConfigureStaffTrophyMapping();
            ConfigurePositionMapping();
            ConfigureStaffPositionHistoryMapping();
            ConfigureSponsorMapping();
            ConfigureStaffMemberMapping();
            ConfigureMatchTypeMapping();
            ConfigureMatchMapping();
            ConfigureClubTrophyMapping();
            ConfigurePastEventsMapping();
            ConfigureContentMapping();
            ConfigureContentTypeMapping();
            ConfigureNewsMapping();
            ConfigureNewsStatusMapping();
        }

        private void ConfigureNewsStatusMapping()
        {
            CreateMap<NewsStatusDto, NewsStatus>();

            CreateMap<NewsStatus, ResponseNewsStatusDto>();
        }

        private void ConfigureNewsMapping()
        {
            CreateMap<CreateNewsRequestDto, News>()
                .ForMember(news => news.Hashtags,
                opt => opt.MapFrom(news => string.Join('#', news.Hashtags)));

            CreateMap<NewsDto, News>();

            CreateMap<News, ResponseNewsDto>()
                .ForMember(dest => dest.Hashtags,
                opt => opt.MapFrom(src => src.Hashtags.Split('#',StringSplitOptions.None)));
        }

        private void ConfigureContentTypeMapping()
        {
            CreateMap<ContentTypeDto, ContentType>();

            CreateMap<ContentType, ResponseContentTypeDto>();
        }

        private void ConfigureContentMapping()
        {
            CreateMap<ContentDto, Content>();

            CreateMap<Content, ResponseContentDto>();
        }

        private void ConfigureAgeCategoryMapping()
        {
            CreateMap<AgeCategoryDto, AgeCategory>();

            CreateMap<AgeCategory, ResponseAgeCategoryDto>();
        }

        public void ConfigureStaffTypeMapping()
        {
            CreateMap<StaffTypeDto, StaffType>();

            CreateMap<StaffType, ResponseStaffTypeDto>();
        }

        public void ConfigureStaffTrophyMapping()
        {
            CreateMap<StaffTrophyDto, StaffTrophy>();

            CreateMap<StaffTrophy, ResponseStaffTrophyDto>();

            CreateMap<AddTrophyToStaffDto, StaffTrophyPositionHistory>()
                .ForMember(dest => dest.StaffPositionHistoryId, 
                    opt => opt.MapFrom(src => src.StaffPositionId))
                .ForMember(dest => dest.StaffTrophyId, 
                    opt => opt.MapFrom(src => src.TrophyId))
                .ForMember(dest => dest.AcquiredDate,
                    opt => opt.MapFrom(src => src.AcquiredDate.ToUniversalTime()));
        }

        public void ConfigurePositionMapping()
        {
            CreateMap<PositionDto, Position>();

            CreateMap<Position, ResponsePositionDto>();

            CreateMap<Position, ResponseStaffPositionDto>();
        }

        public void ConfigureStaffPositionHistoryMapping()
        {
            CreateMap<StaffPositionHistoryDto, StaffPositionHistory>();

            CreateMap<StaffPositionHistory, ResponseStaffPositionHistoryDto>();
            CreateMap<StaffPositionHistory, StaffPositionHistoryDto>();
        }

        public void ConfigureStaffMemberMapping()
        {
            CreateMap<StaffMemberDto, StaffMember>()
                .ForMember(staff => staff.Birthday,
                    opt => opt.MapFrom(staff => staff.BirthDay.ToUniversalTime()));

            CreateMap<StaffMember, ResponseStaffMemberDto>()
                .ForMember(staffDto => staffDto.Position,
                    opt => opt.MapFrom(staff => staff.StaffPositionHistories.First(position => position.EndTime == default).Position))
                .ForPath(staffDto => staffDto.Position.StartTime,
                    opt => opt.MapFrom(staff => staff.StaffPositionHistories.First(position => position.EndTime == default).StartTime));

            CreateMap<StaffMember, StaffHistoryDto>()
                .ForMember(dest => dest.StaffId,
                    opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.PositionHistory, 
                    opt => opt.MapFrom(src => src.StaffPositionHistories));

            CreateMap<StaffPositionHistory, ResponseStaffPositionHistoryDto>()
                .ForMember(dest => dest.Trophies, 
                    opt => opt.MapFrom(src => src.StaffTrophyPositionHistories))
                .ForMember(dest => dest.EndTime,
                    opt => opt.MapFrom(src => src.EndTime == default ? (DateTime?)null : src.EndTime));

            CreateMap<StaffTrophyPositionHistory, ResponseStaffMemberTrophyDto>()
                .ForMember(dest => dest.Name,
                    opt => opt.MapFrom(src => src.StaffTrophy.Name))
                .ForMember(dest => dest.Id,
                        opt => opt.MapFrom(src => src.StaffTrophy.Id))
                .ForMember(dest => dest.AcquiredDate,
                        opt => opt.MapFrom(src => src.AcquiredDate));
        }

        public void ConfigureSponsorMapping()
        {
            CreateMap<SponsorDto, Sponsor>();

            CreateMap<Sponsor, ResponseSponsorDto>();
        }

        private void ConfigureMatchTypeMapping()
        {
            CreateMap<MatchTypeDto, MatchType>();

            CreateMap<MatchType, ResponseMatchTypeDto>();
        }

        private void ConfigureMatchMapping()
        {
            CreateMap<MatchDto, Match>()
                .ForMember(dest => dest.Date,
                    opt => opt.MapFrom(src => src.Date.ToUniversalTime()))
                .ForMember(dest => dest.Edition,
                    opt => opt.MapFrom(src => src.Event));

            CreateMap<UpdateMatchScoreDto, Match>();

            CreateMap<Match, ResponseMatchDto>()
                .ForMember(dest => dest.Event,
                    opt => opt.MapFrom(src => src.Edition));
        }

        private void ConfigureClubTrophyMapping()
        {
            CreateMap<ClubTrophyDto, ClubTrophy>()
                .ForMember(dest => dest.AcquiredDate,
                    opt => opt.MapFrom(src => src.AcquiredDate.ToUniversalTime()));

            CreateMap<ClubTrophy, ResponseClubTrophyDto>();
        }

        private void ConfigurePastEventsMapping()
        {
            CreateMap<PastEventDto, PastEvent>();

            CreateMap<PastEvent, ResponsePastEventDto>();
        }
    }
}
