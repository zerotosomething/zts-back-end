﻿using FluentValidation;
using ZTS.Volley.Domain.Entities.NewsArticles;

namespace ZTS.Volley.Application.Validators.NewsArticles
{
    public class NewsValidator : AbstractValidator<News>
    {
        private const int MIN_LENGHT = 3;

        public NewsValidator()
        {
            RuleFor(news => news.Title)
                .NotNull()
                .NotEmpty()
                .MinimumLength(MIN_LENGHT);

            RuleFor(news => news.Description)
                .NotNull()
                .NotEmpty()
                .MinimumLength(MIN_LENGHT);

            RuleFor(news => news.Text)
                .NotNull()
                .NotEmpty()
                .MinimumLength(MIN_LENGHT);

            RuleFor(news => news.NewsStatusId)
                .NotNull()
                .NotEmpty();
        }
    }
}
