﻿using FluentValidation;
using System.Text.RegularExpressions;
using ZTS.Volley.Application.Dtos.Sponsors;

namespace ZTS.Volley.Application.Validators.Sponsors
{
    public class SponsorValidator : AbstractValidator<SponsorDto>
    {
        private const int MIN_LENGTH = 2;
        private const int MAX_LENGTH = 100;

        public SponsorValidator()
        {
            RuleFor(sponsor => sponsor.Name)
               .NotEmpty()
               .MinimumLength(MIN_LENGTH)
               .MaximumLength(MAX_LENGTH);

            RuleFor(sponsor => sponsor.Edition)
               .NotEmpty();

            RuleFor(sponsor => sponsor.Edition)
               .NotEmpty()
               .MinimumLength(4)
               .MaximumLength(MAX_LENGTH);

            RuleFor(sponsor => sponsor.Logo)
                .NotNull()
                .NotEmpty()
                .Must(logo =>
                {
                    if (!(logo.Length % 4 == 0) || !Regex.IsMatch(logo, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None))
                    {
                        if (!(logo.Contains("zts.blob")))
                        {
                            return false;
                        }
                    }

                    return true;
                })
                .WithMessage("A sponsor must have a logo atttached")
                .WithName("logo");
        }
    }
}
