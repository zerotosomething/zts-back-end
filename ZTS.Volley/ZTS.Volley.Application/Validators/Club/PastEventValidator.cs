﻿using FluentValidation;
using ZTS.Volley.Application.Dtos.Club.PastEvents;

namespace ZTS.Volley.Application.Validators.Club
{
    public class PastEventValidator : AbstractValidator<PastEventDto>
    {
        private const int MIN_LENGTH = 3;
        private const int MAX_LENGTH = 100;

        public PastEventValidator()
        {
            RuleFor(pastEvent => pastEvent.Title)
                .NotEmpty()
                .MinimumLength(MIN_LENGTH)
                .MaximumLength(MAX_LENGTH);

            RuleFor(pastEvent => pastEvent.Description)
                .NotEmpty();
        }
    }
}
