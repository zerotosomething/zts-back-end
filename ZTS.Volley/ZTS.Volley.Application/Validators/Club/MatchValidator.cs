﻿using FluentValidation;
using System.Text.RegularExpressions;
using ZTS.Volley.Application.Dtos.Club.Match;

namespace ZTS.Volley.Application.Validators.Club
{
    public class MatchValidator : AbstractValidator<MatchDto>
    {
        private const int MAX_LENGTH = 100;

        public MatchValidator()
        {
            RuleFor(match => match.Event)
                .NotEmpty()
                .MinimumLength(4)
                .MaximumLength(MAX_LENGTH);

            RuleFor(match => match.Location)
                .NotEmpty();

            RuleFor(match => match.Link);

            RuleFor(match => match.HomeTeam)
                .NotEmpty();

            RuleFor(match => match.AwayTeam)
                .NotEmpty();

            RuleFor(match => match.HomeTeamScore)
                .GreaterThan(-1);

            RuleFor(match => match.AwayTeamScore)
               .GreaterThan(-1);

            RuleFor(match => match.HomeTeamLogo)
                .NotNull()
                .NotEmpty()
                .Must(homeTeamLogo =>
                {
                    if (!(homeTeamLogo.Length % 4 == 0) || !Regex.IsMatch(homeTeamLogo, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None))
                    {
                        if (!(homeTeamLogo.Contains("zts.blob")))
                        {
                            return false;
                        }
                    }

                    return true;
                })
                .WithMessage("A team must have a logo")
                .WithName("profileImage");
                
            RuleFor(match => match.AwayTeamLogo)
                .NotNull()
                .NotEmpty()
                .Must(awayTeamLogo =>
                {
                    if (!(awayTeamLogo.Length % 4 == 0) || !Regex.IsMatch(awayTeamLogo, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None))
                    {
                        if (!(awayTeamLogo.Contains("zts.blob")))
                        {
                            return false;
                        }
                    }

                    return true;
                })
                .WithMessage("A team must have a logo")
                .WithName("profileImage");
        }
    }
}
