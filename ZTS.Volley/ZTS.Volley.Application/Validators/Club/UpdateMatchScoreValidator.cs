﻿using FluentValidation;
using ZTS.Volley.Application.Dtos.Club.Match;

namespace ZTS.Volley.Application.Validators.Club
{
    public class UpdateMatchScoreValidator : AbstractValidator<UpdateMatchScoreDto>
    {
        public UpdateMatchScoreValidator()
        {
            RuleFor(match => match.HomeTeamScore)
                .GreaterThan(-1);

            RuleFor(match => match.AwayTeamScore)
               .GreaterThan(-1);
        }
    }
}
