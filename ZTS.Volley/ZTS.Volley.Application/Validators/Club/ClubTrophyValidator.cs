﻿using FluentValidation;
using ZTS.Volley.Application.Dtos.Club.ClubTrophy;

namespace ZTS.Volley.Application.Validators.Club
{
    public class ClubTrophyValidator : AbstractValidator<ClubTrophyDto>
    {
        private const int MIN_LENGTH = 3;
        private const int MAX_LENGTH = 100;

        public ClubTrophyValidator()
        {
            RuleFor(clubTrophy => clubTrophy.Name)
                .NotEmpty()
                .MinimumLength(MIN_LENGTH)
                .MaximumLength(MAX_LENGTH);

            RuleFor(clubTrophy => clubTrophy.Championship)
                .NotEmpty()
                .MinimumLength(MIN_LENGTH);
        }
    }
}
