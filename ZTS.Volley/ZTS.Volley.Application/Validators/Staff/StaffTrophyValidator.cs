﻿using FluentValidation;
using ZTS.Volley.Application.Dtos.Staff.StaffTrophy;

namespace ZTS.Volley.Application.Validators.Staff
{
    public class StaffTrophyValidator : AbstractValidator<StaffTrophyDto>
    {
        private const int MIN_LENGTH = 5;
        private const int MAX_LENGTH = 100;

        public StaffTrophyValidator()
        {
            RuleFor(enumDto => enumDto.Name)
                .NotNull()
                .NotEmpty()
                .MinimumLength(MIN_LENGTH)
                .MaximumLength(MAX_LENGTH);
        }
    }
}
