﻿using FluentValidation;
using ZTS.Volley.Application.Dtos.Staff.StaffMember;

namespace ZTS.Volley.Application.Validators.Staff
{
    public class StaffMemberValidator : AbstractValidator<StaffMemberDto>
    {
        private const int MIN_LENGTH = 3;
        private const int MAX_LENGTH = 100;
        private const int MIN_HEIGHT = 50;
        private const int MAX_HEIGHT = 250;
        private const int MAX_LENGTH_COUNTRY_CODE = 4;
        private const int MIN_LENGTH_COUNTRY_CODE = 2;

        public StaffMemberValidator()
        {
            RuleFor(staff => staff.FirstName)
                .NotNull()
                .NotEmpty()
                .MinimumLength(MIN_LENGTH)
                .MaximumLength(MAX_LENGTH);

            RuleFor(staff => staff.LastName)
                .NotNull()
                .NotEmpty()
                .MinimumLength(MIN_LENGTH)
                .MaximumLength(MAX_LENGTH);

            RuleFor(staff => staff.Height)
                .GreaterThan(MIN_HEIGHT)
                .LessThan(MAX_HEIGHT);

            RuleFor(staff => staff.Country)
                .NotNull()
                .NotEmpty()
                .MinimumLength(MIN_LENGTH)
                .MaximumLength(MAX_LENGTH);

            RuleFor(staff => staff.CountryCode)
                .NotNull()
                .NotEmpty()
                .MinimumLength(MIN_LENGTH_COUNTRY_CODE)
                .MaximumLength(MAX_LENGTH_COUNTRY_CODE);

            RuleFor(staff => staff)
                .Must(staff =>
                {
                    // Type 1 - jucator Pozitia 7 - Antrenor
                    if (staff.StaffTypeId == 1 && staff.Position.Id == 7)
                    {
                        return false;
                    }

                    return true;
                })
                .WithMessage("A Staff Type id 1 - player cannot have a Position id 7 - coach")
                .WithName("staffTypeId");
        }
    }
}
