﻿using FluentValidation;
using ZTS.Volley.Application.Dtos.Interfaces;

namespace ZTS.Volley.Application.Validators.Enums
{
    public class EnumValidator<T> : AbstractValidator<T> where T : IEnumDto
    {
        private const int MIN_LENGTH = 2;
        private const int MAX_LENGTH = 100;

        public EnumValidator()
        {
            RuleFor(enumDto => enumDto.Name)
                .NotNull()
                .NotEmpty()
                .MinimumLength(MIN_LENGTH)
                .MaximumLength(MAX_LENGTH);
        }
    }
}
