﻿namespace ZTS.Volley.Application.Dtos
{
    public class ResponseAgeCategoryDto : AgeCategoryDto
    {
        public int Id { get; set; }
    }
}
