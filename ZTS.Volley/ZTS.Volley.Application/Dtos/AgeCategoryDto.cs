﻿using ZTS.Volley.Application.Dtos.Interfaces;

namespace ZTS.Volley.Application.Dtos
{
    public class AgeCategoryDto : IEnumDto
    {
        public string Name { get; set; }
    }
}
