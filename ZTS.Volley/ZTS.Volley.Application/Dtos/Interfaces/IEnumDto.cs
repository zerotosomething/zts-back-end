﻿namespace ZTS.Volley.Application.Dtos.Interfaces
{
    public interface IEnumDto
    {
        string Name { get; set; }
    }
}
