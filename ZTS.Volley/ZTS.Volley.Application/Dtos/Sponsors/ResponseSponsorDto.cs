﻿namespace ZTS.Volley.Application.Dtos.Sponsors
{
    public class ResponseSponsorDto : SponsorDto
    {
        public Guid Id { get; set; }
    }
}
