﻿namespace ZTS.Volley.Application.Dtos.Sponsors
{
    public class SponsorDto
    {
        public string Name { get; set; }
        public string Logo { get; set; }
        public string Url { get; set; }
        public string Edition { get; set; }
    }
}
