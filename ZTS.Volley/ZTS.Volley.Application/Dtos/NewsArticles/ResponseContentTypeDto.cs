﻿namespace ZTS.Volley.Application.Dtos.NewsArticles
{
    public class ResponseContentTypeDto : ContentTypeDto
    {
        public int Id { get; set; }
    }
}
