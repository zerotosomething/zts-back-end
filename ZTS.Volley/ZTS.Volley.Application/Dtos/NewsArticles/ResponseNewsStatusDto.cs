﻿namespace ZTS.Volley.Application.Dtos.NewsArticles
{
    public class ResponseNewsStatusDto : NewsStatusDto
    {
        public int Id { get; set; }
    }
}
