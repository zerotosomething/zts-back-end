﻿namespace ZTS.Volley.Application.Dtos.NewsArticles
{
    public class ContentDto
    {
        public string Data { get; set; }
        public int ContentTypeId { get; set; }
        public Guid NewsId { get; set; }
    }
}
