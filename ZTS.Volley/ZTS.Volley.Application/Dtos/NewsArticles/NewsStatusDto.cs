﻿namespace ZTS.Volley.Application.Dtos.NewsArticles
{
    public class NewsStatusDto
    {
        public string Name { get; set; }
    }
}
