﻿namespace ZTS.Volley.Application.Dtos.NewsArticles
{
    public class ContentTypeDto
    {
        public string Name { get; set; }
    }
}
