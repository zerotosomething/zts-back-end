﻿namespace ZTS.Volley.Application.Dtos.NewsArticles
{
    public class ResponseContentDto : ContentDto
    {
        public Guid Id { get; set; }
    }
}
