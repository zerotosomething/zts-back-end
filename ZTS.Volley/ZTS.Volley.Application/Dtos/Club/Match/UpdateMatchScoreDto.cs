﻿namespace ZTS.Volley.Application.Dtos.Club.Match
{
    public class UpdateMatchScoreDto
    {
        public int HomeTeamScore { get; set; }
        public int AwayTeamScore { get; set; }
    }
}
