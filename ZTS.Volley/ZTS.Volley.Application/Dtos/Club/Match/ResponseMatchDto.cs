﻿namespace ZTS.Volley.Application.Dtos.Club.Match
{
    public class ResponseMatchDto
    {
        public Guid Id { get; set; }
        public string Event { get; set; }
        public DateTime Date { get; set; }
        public string Location { get; set; }
        public string Link { get; set; }
        public string HomeTeam { get; set; }
        public string HomeTeamLogo { get; set; }
        public string AwayTeam { get; set; }
        public string AwayTeamLogo { get; set; }
        public int HomeTeamScore { get; set; } = 0;
        public int AwayTeamScore { get; set; } = 0;
        public ResponseMatchTypeDto MatchType { get; set; }
        public ResponseAgeCategoryDto AgeCategory { get; set; }
    }
}
