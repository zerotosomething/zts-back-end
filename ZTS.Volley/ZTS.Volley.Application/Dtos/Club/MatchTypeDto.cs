﻿using ZTS.Volley.Application.Dtos.Interfaces;

namespace ZTS.Volley.Application.Dtos.Club
{
    public class MatchTypeDto : IEnumDto
    {
        public string Name { get; set; }
    }
}
