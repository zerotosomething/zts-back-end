﻿namespace ZTS.Volley.Application.Dtos.Club
{
    public class ResponseMatchTypeDto : MatchTypeDto
    {
        public int Id { get; set; }
    }
}
