﻿namespace ZTS.Volley.Application.Dtos.Club.PastEvents
{
    public class ResponsePastEventDto : PastEventDto
    {
        public Guid Id { get; set; }
    }
}
