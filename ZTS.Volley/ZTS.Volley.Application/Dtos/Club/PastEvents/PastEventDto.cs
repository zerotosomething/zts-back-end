﻿namespace ZTS.Volley.Application.Dtos.Club.PastEvents
{
    public class PastEventDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
    }
}
