﻿namespace ZTS.Volley.Application.Dtos.Club.ClubTrophy
{
    public class ClubTrophyDto
    {
        public string Name { get; set; }
        public DateTime AcquiredDate { get; set; }
        public string Championship { get; set; }
        public int AgeCategoryId { get; set; }
    }
}
