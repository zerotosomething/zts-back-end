﻿namespace ZTS.Volley.Application.Dtos.Club.ClubTrophy
{
    public class ResponseClubTrophyDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime AcquiredDate { get; set; }
        public string Championship { get; set; }
        public ResponseAgeCategoryDto AgeCategory { get; set; }
    }
}
