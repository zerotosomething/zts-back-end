﻿namespace ZTS.Volley.Application.Dtos.Staff.StaffType
{
    public class ResponseStaffTypeDto : StaffTypeDto
    {
        public int Id { get; set; }
    }
}
