﻿using ZTS.Volley.Application.Dtos.Interfaces;

namespace ZTS.Volley.Application.Dtos.Staff.StaffType
{
    public class StaffTypeDto : IEnumDto
    {
        public string Name { get; set; }
    }
}
