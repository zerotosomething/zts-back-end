﻿using ZTS.Volley.Application.Dtos.Interfaces;

namespace ZTS.Volley.Application.Dtos.Staff.Position
{
    public class PositionDto : IEnumDto
    {
        public string Name { get; set; }
    }
}
