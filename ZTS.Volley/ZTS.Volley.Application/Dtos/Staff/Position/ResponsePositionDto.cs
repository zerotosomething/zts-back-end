﻿namespace ZTS.Volley.Application.Dtos.Staff.Position
{
    public class ResponsePositionDto : PositionDto
    {
        public int Id { get; set; }
    }
}
