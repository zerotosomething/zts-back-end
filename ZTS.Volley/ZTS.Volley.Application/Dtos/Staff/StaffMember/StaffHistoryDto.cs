﻿using ZTS.Volley.Application.Dtos.Staff.StaffPositionHistory;

namespace ZTS.Volley.Application.Dtos.Staff.StaffMember
{
    public class StaffHistoryDto
    {
        public Guid StaffId { get; set; }
        public List<ResponseStaffPositionHistoryDto> PositionHistory { get; set; }
    }
}
