﻿using ZTS.Volley.Application.Dtos.Staff.StaffType;

namespace ZTS.Volley.Application.Dtos.Staff.StaffMember
{
    public class ResponseStaffMemberDto
    {
        public Guid Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime BirthDay { get; set; }
        public int Height { get; set; }
        public string? Description { get; set; }
        public string ProfileImage { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public ResponseStaffPositionDto Position { get; set; }
        public ResponseAgeCategoryDto AgeCategory { get; set; }
        public ResponseStaffTypeDto StaffType { get; set; }
    }
}
