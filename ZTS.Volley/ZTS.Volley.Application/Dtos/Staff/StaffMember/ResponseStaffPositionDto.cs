﻿namespace ZTS.Volley.Application.Dtos.Staff.StaffMember
{
    public class ResponseStaffPositionDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartTime { get; set; }
    }
}
