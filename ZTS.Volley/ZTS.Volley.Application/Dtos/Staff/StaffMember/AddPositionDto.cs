﻿namespace ZTS.Volley.Application.Dtos.Staff.StaffMember
{
    public class AddPositionDto
    {
        public int Id { get; set; }
        public DateTime StartTime { get; set; }
    }
}
