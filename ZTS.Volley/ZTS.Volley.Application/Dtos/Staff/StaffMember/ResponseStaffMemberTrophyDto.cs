﻿namespace ZTS.Volley.Application.Dtos.Staff.StaffMember
{
    public class ResponseStaffMemberTrophyDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime AcquiredDate { get; set; }
    }
}
