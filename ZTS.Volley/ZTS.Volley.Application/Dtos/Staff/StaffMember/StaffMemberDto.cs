﻿namespace ZTS.Volley.Application.Dtos.Staff.StaffMember
{
    public class StaffMemberDto
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime BirthDay { get; set; }
        public int Height { get; set; }
        public string? Description { get; set; }
        public string ProfileImage { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public int AgeCategoryId { get; set; }
        public int StaffTypeId { get; set; }
        public AddPositionDto Position { get; set; }
    }
}
