﻿namespace ZTS.Volley.Application.Dtos.Staff.StaffPositionHistory
{
    public class StaffPositionHistoryDto
    {
        public Guid StaffMemberId { get; set; }
        public int PositionId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
    }
}
