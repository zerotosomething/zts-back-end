﻿using ZTS.Volley.Application.Dtos.Staff.Position;
using ZTS.Volley.Application.Dtos.Staff.StaffMember;

namespace ZTS.Volley.Application.Dtos.Staff.StaffPositionHistory
{
    public class ResponseStaffPositionHistoryDto
    {
        public Guid Id { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public ResponsePositionDto Position { get; set; }
        public List<ResponseStaffMemberTrophyDto> Trophies { get; set; }
    }
}
