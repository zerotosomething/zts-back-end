﻿namespace ZTS.Volley.Application.Dtos.Staff.StaffTrophy
{
    public class AddTrophyToStaffDto
    {
        public Guid StaffPositionId { get; set; }
        public Guid TrophyId { get; set; }
        public DateTime AcquiredDate { get; set; }
    }
}
