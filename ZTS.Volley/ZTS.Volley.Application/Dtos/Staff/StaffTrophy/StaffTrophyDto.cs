﻿namespace ZTS.Volley.Application.Dtos.Staff.StaffTrophy
{
    public class StaffTrophyDto
    {
        public string Name { get; set; }
    }
}
