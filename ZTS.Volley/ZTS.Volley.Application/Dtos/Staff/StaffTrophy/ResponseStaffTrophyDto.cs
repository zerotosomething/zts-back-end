﻿namespace ZTS.Volley.Application.Dtos.Staff.StaffTrophy
{
    public class ResponseStaffTrophyDto : StaffTrophyDto
    {
        public Guid Id { get; set; }
    }
}
