﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZTS.Volley.Domain.Entities;

namespace ZTS.Volley.Domain.Configurations
{
    public class BaseEnumEntityConfiguration<TEntity> : IEntityTypeConfiguration<TEntity> where TEntity : BaseEnumEntity
    {
        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.IsDeleted)
                .IsRequired();
        }
    }
}
