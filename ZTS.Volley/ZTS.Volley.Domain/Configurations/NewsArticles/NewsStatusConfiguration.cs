﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZTS.Volley.Domain.Entities.NewsArticles;

namespace ZTS.Volley.Domain.Configurations.NewsArticles
{
    public class NewsStatusConfiguration : BaseEnumEntityConfiguration<NewsStatus>
    {
        public override void Configure(EntityTypeBuilder<NewsStatus> builder)
        {
            base.Configure(builder);

            builder.Property(entity => entity.Name)
                .IsRequired()
                .HasMaxLength(20);
        }
    }
}
