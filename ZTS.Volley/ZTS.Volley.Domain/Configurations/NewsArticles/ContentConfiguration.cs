﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZTS.Volley.Domain.Entities.NewsArticles;

namespace ZTS.Volley.Domain.Configurations.NewsArticles
{
    public class ContentConfiguration : BaseEntityConfiguration<Content>
    {
        public override void Configure(EntityTypeBuilder<Content> builder)
        {
            base.Configure(builder);

            builder.HasOne(content => content.ContentType)
                .WithMany(contentType => contentType.Contents)
                .HasForeignKey(content => content.ContentTypeId);

            builder.HasOne(content => content.News)
                .WithMany(news => news.Contents)
                .HasForeignKey(content => content.NewsId);

            builder.Property(entity => entity.Data)
                .IsRequired();
        }
    }
}
