﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZTS.Volley.Domain.Entities.NewsArticles;

namespace ZTS.Volley.Domain.Configurations.NewsArticles
{
    public class NewsConfiguration : BaseEntityConfiguration<News>
    {
        public override void Configure(EntityTypeBuilder<News> builder)
        {
            base.Configure(builder);

            builder.HasOne(news => news.NewsStatus)
                .WithMany(newsStatus => newsStatus.News)
                .HasForeignKey(news => news.NewsStatusId);

            builder.Property(entity => entity.Title)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(entity => entity.Description)
                .IsRequired();

            builder.Property(entity => entity.Text)
                .IsRequired();

            builder.Property(entity => entity.Hashtags)
                .IsRequired(false)
                .HasMaxLength(200);

            builder.Property(entity => entity.CreationDate)
                .IsRequired()
                .HasConversion(date => date, date => DateTime.SpecifyKind(date, DateTimeKind.Utc));

            builder.Property(entity => entity.PublishDate)
                .IsRequired(false)
                .HasConversion(date => date, date => date.HasValue ? DateTime.SpecifyKind(date.Value, DateTimeKind.Utc) : date);
        }
    }
}
