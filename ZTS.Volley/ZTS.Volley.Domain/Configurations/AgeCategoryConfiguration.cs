﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZTS.Volley.Domain.Entities;

namespace ZTS.Volley.Domain.Configurations
{
    public class AgeCategoryConfiguration : BaseEnumEntityConfiguration<AgeCategory>
    {
        public override void Configure(EntityTypeBuilder<AgeCategory> builder)
        {
            base.Configure(builder);

            builder.Property(ageCategory => ageCategory.Name)
                .IsRequired()
                .HasMaxLength(25);
        }
    }
}
