﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZTS.Volley.Domain.Entities.Club;

namespace ZTS.Volley.Domain.Configurations.Club
{
    public class PastEventConfiguration : BaseEntityConfiguration<PastEvent>
    {
        public override void Configure(EntityTypeBuilder<PastEvent> builder)
        {
            base.Configure(builder);

            builder.Property(eventEntity => eventEntity.Title)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(eventEntity => eventEntity.Description)
                .IsRequired();

            builder.Property(eventEntity => eventEntity.Date)
                .IsRequired()
                .HasConversion(date => date, date => DateTime.SpecifyKind(date, DateTimeKind.Utc));
        }
    }
}
