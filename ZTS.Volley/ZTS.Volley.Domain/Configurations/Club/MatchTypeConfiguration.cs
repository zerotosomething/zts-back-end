﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MatchType = ZTS.Volley.Domain.Entities.Club.MatchType;

namespace ZTS.Volley.Domain.Configurations.Club
{
    public class MatchTypeConfiguration : BaseEnumEntityConfiguration<MatchType>
    {
        public override void Configure(EntityTypeBuilder<MatchType> builder)
        {
            base.Configure(builder);

            builder.Property(matchType => matchType.Name)
                .IsRequired()
                .HasMaxLength(25);
        }
    }
}
