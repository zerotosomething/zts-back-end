﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZTS.Volley.Domain.Entities.Club;

namespace ZTS.Volley.Domain.Configurations.Club
{
    public class ClubTrophyConfiguration : BaseEntityConfiguration<ClubTrophy>
    {
        public override void Configure(EntityTypeBuilder<ClubTrophy> builder)
        {
            base.Configure(builder);

            builder.Property(clubTrophy => clubTrophy.Name)
                .IsRequired();

            builder.Property(clubTrophy => clubTrophy.AcquiredDate)
                .IsRequired()
                .HasConversion(date => date, date => DateTime.SpecifyKind(date, DateTimeKind.Utc));

            builder.Property(clubTrophy => clubTrophy.Championship)
                .IsRequired();

            builder.HasOne(clubTrophy => clubTrophy.AgeCategory)
                .WithMany(ageCategory => ageCategory.ClubTrophies)
                .HasForeignKey(clubTrophy => clubTrophy.AgeCategoryId);
        }
    }
}
