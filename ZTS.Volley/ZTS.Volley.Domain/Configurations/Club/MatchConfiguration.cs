﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZTS.Volley.Domain.Entities.Club;

namespace ZTS.Volley.Domain.Configurations.Club
{
    public class MatchConfiguration : BaseEntityConfiguration<Match>
    {
        public override void Configure(EntityTypeBuilder<Match> builder)
        {
            base.Configure(builder);

            builder.Property(match => match.Edition)
                .IsRequired()
                .HasMaxLength(128);

            builder.Property(match => match.Date)
                .IsRequired()
                .HasConversion(date => date, date => DateTime.SpecifyKind(date, DateTimeKind.Utc));

            builder.Property(match => match.Location)
                .IsRequired();

            builder.Property(match => match.Link)
                .IsRequired();

            builder.Property(match => match.HomeTeam)
                .IsRequired();

            builder.Property(match => match.HomeTeamLogo)
                .IsRequired();

            builder.Property(match => match.AwayTeam)
                .IsRequired();

            builder.Property(match => match.AwayTeamLogo)
                .IsRequired();

            builder.Property(match => match.HomeTeamScore)
                .IsRequired();

            builder.Property(match => match.AwayTeamScore)
                .IsRequired();

            builder.HasOne(match => match.MatchType)
                .WithMany(matchType => matchType.Matches)
                .HasForeignKey(match => match.MatchTypeId);

            builder.HasOne(match => match.AgeCategory)
                .WithMany(ageCategory => ageCategory.Matches)
                .HasForeignKey(match => match.AgeCategoryId);
        }
    }
}
