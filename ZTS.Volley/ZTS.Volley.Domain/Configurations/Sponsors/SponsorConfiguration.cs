﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZTS.Volley.Domain.Entities.Sponsors;

namespace ZTS.Volley.Domain.Configurations.Sponsors
{
    public class SponsorConfiguration : BaseEntityConfiguration<Sponsor>
    {
        public override void Configure(EntityTypeBuilder<Sponsor> builder)
        {
            base.Configure(builder);

            builder.Property(sponsor => sponsor.Name)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(sponsor => sponsor.Logo)
                .IsRequired();

            builder.Property(sponsor => sponsor.Url);

            builder.Property(sponsor => sponsor.Edition)
                .IsRequired();
        }
    }
}
