﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZTS.Volley.Domain.Entities.Staff;

namespace ZTS.Volley.Domain.Configurations.Staff
{
    public class PositionConfiguration : BaseEnumEntityConfiguration<Position>
    {
        public override void Configure(EntityTypeBuilder<Position> builder)
        {
            base.Configure(builder);

            builder.Property(position => position.Name)
                .IsRequired()
                .HasMaxLength(30);
        }
    }
}
