﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZTS.Volley.Domain.Entities.Staff;

namespace ZTS.Volley.Domain.Configurations.Staff
{
    public class StaffPositionHistoryConfiguration : BaseEntityConfiguration<StaffPositionHistory>
    {
        public override void Configure(EntityTypeBuilder<StaffPositionHistory> builder)
        {
            base.Configure(builder);

            builder.Property(staffPositionHistory => staffPositionHistory.StartTime)
                .IsRequired();

            builder.Property(staffPositionHistory => staffPositionHistory.EndTime)
                .IsRequired();

            builder.HasOne(staffPositionHistory => staffPositionHistory.StaffMember)
                .WithMany(staffMember => staffMember.StaffPositionHistories)
                .HasForeignKey(staffPositionHistory => staffPositionHistory.StaffMemberId);

            builder.HasOne(staffPositionHistory => staffPositionHistory.Position)
                .WithMany(position => position.StaffPositionHistories)
                .HasForeignKey(staffPositionHistory => staffPositionHistory.PositionId);
        }
    }
}
