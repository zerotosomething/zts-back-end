﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZTS.Volley.Domain.Entities.Staff.JoiningEntities;

namespace ZTS.Volley.Domain.Configurations.Staff
{
    public class StaffTrophyPositionHistoryConfiguration : IEntityTypeConfiguration<StaffTrophyPositionHistory>
    {
        public void Configure(EntityTypeBuilder<StaffTrophyPositionHistory> builder)
        {
            builder
                .HasKey(staffTrophyPositionHistory => new { staffTrophyPositionHistory.StaffTrophyId, staffTrophyPositionHistory.StaffPositionHistoryId });

            builder
                .HasOne(staffTrophyPositionHistory => staffTrophyPositionHistory.StaffTrophy)
                .WithMany(staffTrophy => staffTrophy.StaffTrophyPositionHistories)
                .HasForeignKey(staffTrophyPositionHistory => staffTrophyPositionHistory.StaffTrophyId);

            builder
                .HasOne(staffTrophyPositionHistory => staffTrophyPositionHistory.StaffPositionHistory)
                .WithMany(staffPositionHistory => staffPositionHistory.StaffTrophyPositionHistories)
                .HasForeignKey(staffTrophyPositionHistory => staffTrophyPositionHistory.StaffPositionHistoryId);

            builder.Property(staffTrophyPositionHistory => staffTrophyPositionHistory.AcquiredDate)
                .IsRequired();
        }
    }
}
