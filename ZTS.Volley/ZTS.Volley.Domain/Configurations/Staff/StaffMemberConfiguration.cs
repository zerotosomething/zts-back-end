﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZTS.Volley.Domain.Entities.Staff;

namespace ZTS.Volley.Domain.Configurations.Staff
{
    public class StaffMemberConfiguration : BaseEntityConfiguration<StaffMember>
    {
        public override void Configure(EntityTypeBuilder<StaffMember> builder)
        {
            base.Configure(builder);

            builder.Property(staffMember => staffMember.LastName)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(staffMember => staffMember.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(staffMember => staffMember.Birthday)
                .IsRequired()
                .HasConversion(date => date, date => DateTime.SpecifyKind(date, DateTimeKind.Utc));

            builder.Property(staffMember => staffMember.Height)
                .IsRequired();

            builder.Property(staffMember => staffMember.Description);

            builder.Property(staffMember => staffMember.Country)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(staffMember => staffMember.CountryCode)
                .IsRequired()
                .HasMaxLength(4);

            builder.Property(staffMember => staffMember.ProfileImage)
                .IsRequired();

            builder.HasOne(staffMember => staffMember.StaffType)
                .WithMany(staffType => staffType.StaffMembers)
                .HasForeignKey(staffMember => staffMember.StaffTypeId);

            builder.HasOne(staffMember => staffMember.AgeCategory)
                .WithMany(ageCategory => ageCategory.StaffMembers)
                .HasForeignKey(staffMember => staffMember.AgeCategoryId);
        }
    }
}
