﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZTS.Volley.Domain.Entities.Staff;

namespace ZTS.Volley.Domain.Configurations.Staff
{
    public class StaffTypeConfiguration : BaseEnumEntityConfiguration<StaffType>
    {
        public override void Configure(EntityTypeBuilder<StaffType> builder)
        {
            base.Configure(builder);

            builder.Property(staffType => staffType.Name)
                .IsRequired();
        }
    }
}
