﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZTS.Volley.Domain.Entities.Staff;

namespace ZTS.Volley.Domain.Configurations.Staff
{
    public class StaffTrophyConfiguration : BaseEntityConfiguration<StaffTrophy>
    {
        public override void Configure(EntityTypeBuilder<StaffTrophy> builder)
        {
            base.Configure(builder);

            builder.Property(staffTrophy => staffTrophy.Name)
                .IsRequired();
        }
    }
}
