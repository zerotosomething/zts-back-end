﻿namespace ZTS.Volley.Domain.Entities
{
    public record BaseEnumEntity
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}
