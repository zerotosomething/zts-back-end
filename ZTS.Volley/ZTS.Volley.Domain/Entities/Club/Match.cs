﻿namespace ZTS.Volley.Domain.Entities.Club
{
    public record Match : BaseEntity
    {
        public string Edition { get; set; }
        public DateTime Date { get; set; }
        public string Location { get; set; }
        public string Link { get; set; }
        public string HomeTeam { get; set; }
        public string HomeTeamLogo { get; set; }
        public string AwayTeam { get; set; }
        public string AwayTeamLogo { get; set; }
        public int HomeTeamScore { get; set; }
        public int AwayTeamScore { get; set; }
        public int MatchTypeId { get; set; }
        public MatchType MatchType { get; set; }
        public int AgeCategoryId { get; set; }
        public AgeCategory AgeCategory { get; set; }
    }
}
