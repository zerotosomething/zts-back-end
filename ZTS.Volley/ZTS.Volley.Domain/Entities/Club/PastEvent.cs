﻿namespace ZTS.Volley.Domain.Entities.Club
{
    public record PastEvent : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
    }
}
