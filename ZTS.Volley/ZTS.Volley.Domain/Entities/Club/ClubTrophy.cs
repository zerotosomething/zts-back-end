﻿namespace ZTS.Volley.Domain.Entities.Club
{
    public record ClubTrophy : BaseEntity
    {
        public string Name { get; set; }
        public DateTime AcquiredDate { get; set; }
        public string Championship { get; set; }
        public int AgeCategoryId { get; set; }
        public AgeCategory AgeCategory { get; set; }
    }
}
