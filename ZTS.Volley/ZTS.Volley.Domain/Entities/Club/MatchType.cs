﻿namespace ZTS.Volley.Domain.Entities.Club
{
    public record MatchType : BaseEnumEntity
    {
        public string Name { get; set; }
        public List<Match> Matches { get; set; }
    }
}
