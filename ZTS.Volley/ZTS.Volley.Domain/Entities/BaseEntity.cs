﻿namespace ZTS.Volley.Domain.Entities
{
    public record BaseEntity
    {
        public Guid Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}
