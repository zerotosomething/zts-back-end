﻿namespace ZTS.Volley.Domain.Entities.Sponsors
{
    public record Sponsor : BaseEntity
    {
        public string Name { get; set; }
        public string Logo { get; set; }
        public string Url { get; set; }
        public string Edition { get; set; }
    }
}
