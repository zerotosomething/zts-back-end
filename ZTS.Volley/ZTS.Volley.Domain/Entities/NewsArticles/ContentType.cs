﻿namespace ZTS.Volley.Domain.Entities.NewsArticles
{
    public record ContentType : BaseEnumEntity
    {
        public string Name { get; set; }
        public List<Content>? Contents { get; set; }
    }
}
