﻿namespace ZTS.Volley.Domain.Entities.NewsArticles
{
    public record Content : BaseEntity
    {
        public string Data { get; set; }
        public int ContentTypeId { get; set; }
        public ContentType ContentType { get; set; }
        public Guid NewsId { get; set; }
        public News News { get; set; }
    }
}
