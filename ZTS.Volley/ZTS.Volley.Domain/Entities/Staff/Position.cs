﻿namespace ZTS.Volley.Domain.Entities.Staff
{
    public record Position : BaseEnumEntity
    {
        public string Name { get; set; }
        public List<StaffPositionHistory> StaffPositionHistories { get; set; }
    }
}
