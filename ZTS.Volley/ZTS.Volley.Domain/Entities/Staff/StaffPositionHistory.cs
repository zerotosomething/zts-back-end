﻿using ZTS.Volley.Domain.Entities.Staff.JoiningEntities;

namespace ZTS.Volley.Domain.Entities.Staff
{
    public record StaffPositionHistory : BaseEntity
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public Guid StaffMemberId { get; set; }
        public StaffMember StaffMember { get; set; }
        public int PositionId { get; set; }
        public Position Position { get; set; }
        public List<StaffTrophyPositionHistory> StaffTrophyPositionHistories { get; set; }
    }
}
