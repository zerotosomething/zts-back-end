﻿namespace ZTS.Volley.Domain.Entities.Staff
{
    public record StaffMember : BaseEntity
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime Birthday { get; set; }
        public int Height { get; set; }
        public string? Description { get; set; }
        public string ProfileImage { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public int StaffTypeId { get; set; }
        public StaffType StaffType { get; set; }
        public int AgeCategoryId { get; set; }
        public AgeCategory AgeCategory { get; set; }
        public List<StaffPositionHistory> StaffPositionHistories { get; set; }
    }
}
