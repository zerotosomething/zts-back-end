﻿using ZTS.Volley.Domain.Entities.Staff.JoiningEntities;

namespace ZTS.Volley.Domain.Entities.Staff
{
    public record StaffTrophy : BaseEntity
    {
        public string Name { get; set; }
        public List<StaffTrophyPositionHistory> StaffTrophyPositionHistories { get; set; } = new List<StaffTrophyPositionHistory>();
    }
}
