﻿namespace ZTS.Volley.Domain.Entities.Staff
{
    public record StaffType : BaseEnumEntity
    {
        public string Name { get; set; }
        public List<StaffMember> StaffMembers { get; set; }
    }
}
