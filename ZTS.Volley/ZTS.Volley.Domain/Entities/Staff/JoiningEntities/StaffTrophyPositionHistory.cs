﻿namespace ZTS.Volley.Domain.Entities.Staff.JoiningEntities
{
    public record StaffTrophyPositionHistory
    {
        public Guid StaffTrophyId { get; set; }
        public StaffTrophy StaffTrophy { get; set; }
        public Guid StaffPositionHistoryId { get; set; }
        public StaffPositionHistory StaffPositionHistory { get; set; }
        public DateTime AcquiredDate { get; set; }
    }
}
