﻿using ZTS.Volley.Domain.Entities.Club;
using ZTS.Volley.Domain.Entities.Staff;

namespace ZTS.Volley.Domain.Entities
{
    public record AgeCategory : BaseEnumEntity
    {
        public string Name { get; set; }
        public List<StaffMember> StaffMembers { get; set; }
        public List<Match> Matches { get; set; }
        public List<ClubTrophy> ClubTrophies { get; set; }
    }
}
