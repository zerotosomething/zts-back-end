﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ZTS.Volley.Infrastructure.Migrations
{
    public partial class init7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Position",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[] { 7, false, "Antrenor" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Position",
                keyColumn: "Id",
                keyValue: 7);
        }
    }
}
