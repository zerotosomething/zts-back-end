﻿using Microsoft.EntityFrameworkCore;
using ZTS.Volley.Domain.Configurations;
using ZTS.Volley.Domain.Configurations.Club;
using ZTS.Volley.Domain.Configurations.NewsArticles;
using ZTS.Volley.Domain.Configurations.Sponsors;
using ZTS.Volley.Domain.Configurations.Staff;
using ZTS.Volley.Domain.Entities;
using ZTS.Volley.Domain.Entities.Club;
using ZTS.Volley.Domain.Entities.NewsArticles;
using ZTS.Volley.Domain.Entities.Sponsors;
using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Domain.Entities.Staff.JoiningEntities;
using MatchType = ZTS.Volley.Domain.Entities.Club.MatchType;

namespace ZTS.Volley.Infrastructure.Data
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Match> Match { get; set; }
        public DbSet<PastEvent> PastEvent { get; set; }
        public DbSet<ClubTrophy> ClubTrophy { get; set; }
        public DbSet<MatchType> MatchType { get; set; }
        public DbSet<AgeCategory> AgeCategory { get; set; }
        public DbSet<Position> Position { get; set; }
        public DbSet<StaffMember> StaffMember { get; set; }
        public DbSet<StaffPositionHistory> StaffPositionHistory { get; set; }
        public DbSet<StaffTrophyPositionHistory> StaffTrophyPositionHistory { get; set; }
        public DbSet<StaffTrophy> StaffTrophy { get; set; }
        public DbSet<StaffType> StaffType { get; set; }
        public DbSet<Sponsor> Sponsors { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<NewsStatus> NewsStatus { get; set; }
        public DbSet<Content> Content { get; set; }
        public DbSet<ContentType> ContentType { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            new MatchConfiguration().Configure(builder.Entity<Match>());
            new PastEventConfiguration().Configure(builder.Entity<PastEvent>());
            new ClubTrophyConfiguration().Configure(builder.Entity<ClubTrophy>());
            new MatchTypeConfiguration().Configure(builder.Entity<MatchType>());
            new AgeCategoryConfiguration().Configure(builder.Entity<AgeCategory>());    
            new PositionConfiguration().Configure(builder.Entity<Position>());
            new StaffMemberConfiguration().Configure(builder.Entity<StaffMember>());
            new StaffPositionHistoryConfiguration().Configure(builder.Entity<StaffPositionHistory>());
            new StaffTrophyPositionHistoryConfiguration().Configure(builder.Entity<StaffTrophyPositionHistory>());
            new StaffTypeConfiguration().Configure(builder.Entity<StaffType>());
            new SponsorConfiguration().Configure(builder.Entity<Sponsor>());
            new ContentConfiguration().Configure(builder.Entity<Content>());
            new ContentTypeConfiguration().Configure(builder.Entity<ContentType>());
            new NewsConfiguration().Configure(builder.Entity<News>());
            new NewsStatusConfiguration().Configure(builder.Entity<NewsStatus>());

            AddDefaultEnums(builder);
        }

        private void AddDefaultEnums(ModelBuilder builder)
        {
            AddDefaultPositions(builder);
            AddAgeCategories(builder);
            AddStaffTypes(builder);
            AddMatchType(builder);
            AddContentType(builder);
            AddNewsStatus(builder);
        }

        private void AddNewsStatus(ModelBuilder builder)
        {
            var draft = new NewsStatus { Id = 1, Name = "draft" };
            var published = new NewsStatus { Id = 2, Name = "published" };

            builder.Entity<NewsStatus>().HasData(draft, published);
        }

        private void AddContentType(ModelBuilder builder)
        {
            var photo = new ContentType { Id = 1, Name = "photo" };
            var video = new ContentType { Id = 2, Name = "video" };

            builder.Entity<ContentType>().HasData(photo, video);
        }

        private void AddDefaultPositions(ModelBuilder builder)
        {
            var backRight = new Position { Id = 1, Name = "Spate - Dreapta"};
            var frontRight = new Position { Id = 2, Name = "Fata - Dreapta"};
            var frontCenter = new Position { Id = 3, Name = "Fata - Centru"};
            var frontLeft = new Position { Id = 4, Name = "Fata - Stanga"};
            var backLeft = new Position { Id = 5, Name = "Spate - Stanga"};
            var backCenter = new Position { Id = 6, Name = "Spate - Centru"};
            var coach = new Position { Id = 7, Name = "Antrenor"};

            builder.Entity<Position>().HasData(backRight, frontRight, frontCenter, frontLeft, backLeft, backCenter, coach);
        }

        private void AddAgeCategories(ModelBuilder builder)
        {
            var a1 = new AgeCategory { Id = 1, Name = "A1" };
            var a2Vest = new AgeCategory { Id = 2, Name = "A2 Vest" };
            var a2Est = new AgeCategory { Id = 3, Name = "A2 Est" };
            var juniori = new AgeCategory { Id = 4, Name = "Juniori" };
            var cadeti = new AgeCategory { Id = 5, Name = "Cadeti" };
            var sperante = new AgeCategory { Id = 6, Name = "Sperante" };
            var minivolei = new AgeCategory { Id = 7, Name = "Mini-volei" };

            builder.Entity<AgeCategory>().HasData(a1, a2Vest, a2Est, juniori, cadeti, sperante, minivolei);
        }

        private void AddStaffTypes(ModelBuilder builder)
        {
            var player = new StaffType { Id = 1, Name = "Jucator" };
            var coach = new StaffType { Id = 2, Name = "Antrenor" };

            builder.Entity<StaffType>().HasData(player, coach);
        }

        private void AddMatchType(ModelBuilder builder)
        {
            var friendly = new MatchType { Id = 1, Name = "Amical" };
            var county = new MatchType { Id = 2, Name = "Campionat Judetean" };
            var national = new MatchType { Id = 3, Name = "Campionat National" };
            var international = new MatchType { Id = 4, Name = "Campionat International" };

            builder.Entity<MatchType>().HasData(friendly, county, national, international);
        }
    }
}
