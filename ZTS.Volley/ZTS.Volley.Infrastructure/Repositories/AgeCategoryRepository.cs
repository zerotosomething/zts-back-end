﻿using Microsoft.EntityFrameworkCore;
using ZTS.Volley.Domain.Entities;
using ZTS.Volley.Infrastructure.Data;
using ZTS.Volley.Infrastructure.Repositories.Base;
using ZTS.Volley.Infrastructure.Repositories.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories
{
    public class AgeCategoryRepository : BaseEnumEntityRepository<AgeCategory>, IAgeCategoryRepository
    {
        public AgeCategoryRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext) { }

        public async Task<AgeCategory> GetByName(string name)
        {
            var ageCategories = await applicationDbContext.AgeCategory
                .Include(ageCategory => ageCategory.StaffMembers)
                .FirstOrDefaultAsync(ageCategory => ageCategory.Name == name);

            return ageCategories;
        }
    }
}
