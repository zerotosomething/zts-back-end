﻿using Microsoft.EntityFrameworkCore;
using ZTS.Volley.Domain.Entities.Club;
using ZTS.Volley.Infrastructure.Data;
using ZTS.Volley.Infrastructure.Repositories.Base;
using ZTS.Volley.Infrastructure.Repositories.Club.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Club
{
    public class ClubTrophyRepository : BaseEntityRepository<ClubTrophy>, IClubTrophyRepository
    {
        public ClubTrophyRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
        }

        public override async Task<ClubTrophy?> GetById(Guid id)
        {
            var clubTrophy = await dbSet
                .Where(clubTrophy => clubTrophy.Id == id)
                .Include(clubTrophy => clubTrophy.AgeCategory)
                .FirstOrDefaultAsync();

            return clubTrophy;
        }

        public override async Task<List<ClubTrophy>> GetAll()
        {
            var clubTrophies = await dbSet
                .Where(clubTrophy => clubTrophy.IsDeleted == false)
                .Include(clubTrophy => clubTrophy.AgeCategory)
                .ToListAsync();

            return clubTrophies;
        }
    }
}
