﻿using ZTS.Volley.Infrastructure.Data;
using ZTS.Volley.Infrastructure.Repositories.Base;
using ZTS.Volley.Infrastructure.Repositories.Club.Interfaces;
using MatchType = ZTS.Volley.Domain.Entities.Club.MatchType;

namespace ZTS.Volley.Infrastructure.Repositories.Club
{
    public class MatchTypeRepository : BaseEnumEntityRepository<MatchType>, IMatchTypeRepository
    {
        public MatchTypeRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
        }
    }
}
