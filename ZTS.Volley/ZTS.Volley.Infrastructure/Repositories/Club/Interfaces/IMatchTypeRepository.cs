﻿using ZTS.Volley.Infrastructure.Repositories.Base.Interfaces;
using MatchType = ZTS.Volley.Domain.Entities.Club.MatchType;

namespace ZTS.Volley.Infrastructure.Repositories.Club.Interfaces
{
    public interface IMatchTypeRepository : IBaseEnumEntityRepository<MatchType>
    {

    }
}
