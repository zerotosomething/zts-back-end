﻿using ZTS.Volley.Domain.Entities.Club;
using ZTS.Volley.Infrastructure.Repositories.Base.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Club.Interfaces
{
    public interface IClubTrophyRepository : IBaseEntityRepository<ClubTrophy>
    {

    }
}
