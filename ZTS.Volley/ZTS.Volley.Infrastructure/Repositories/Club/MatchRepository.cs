﻿using Microsoft.EntityFrameworkCore;
using ZTS.Volley.Domain.Entities.Club;
using ZTS.Volley.Infrastructure.Data;
using ZTS.Volley.Infrastructure.Repositories.Base;
using ZTS.Volley.Infrastructure.Repositories.Club.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Club
{
    public class MatchRepository : BaseEntityRepository<Match>, IMatchRepository
    {
        public MatchRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
        }

        public override async Task<Match?> GetById(Guid id)
        {
            var match = await dbSet
                .Where(match => match.IsDeleted == false && match.Id == id)
                .Include(match => match.AgeCategory)
                .Include(match => match.MatchType)
                .FirstOrDefaultAsync();

            return match;
        }

        public override async Task<List<Match>> GetAll()
        {
            var matches = await dbSet
                .Where(match => match.IsDeleted == false)
                .Include(match => match.AgeCategory)
                .Include(match => match.MatchType)
                .OrderByDescending(match => match.Date)
                .ToListAsync();

            return matches;
        }
    }
}
