﻿using ZTS.Volley.Domain.Entities.Club;
using ZTS.Volley.Infrastructure.Data;
using ZTS.Volley.Infrastructure.Repositories.Base;
using ZTS.Volley.Infrastructure.Repositories.Club.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Club
{
    public class PastEventRepository : BaseEntityRepository<PastEvent>, IPastEventRepository
    {
        public PastEventRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
        }
    }
}
