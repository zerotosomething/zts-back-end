﻿using ZTS.Volley.Domain.Entities;
using ZTS.Volley.Infrastructure.Repositories.Base.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Interfaces
{
    public interface IAgeCategoryRepository : IBaseEnumEntityRepository<AgeCategory>
    {
        Task<AgeCategory> GetByName(string name);
    }
}
