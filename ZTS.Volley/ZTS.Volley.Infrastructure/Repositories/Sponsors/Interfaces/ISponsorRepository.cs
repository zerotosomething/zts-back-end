﻿using ZTS.Volley.Domain.Entities.Sponsors;
using ZTS.Volley.Infrastructure.Repositories.Base.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Sponsors.Interfaces
{
    public interface ISponsorRepository : IBaseEntityRepository<Sponsor>
    {
        Task<Sponsor> GetByName(string name);
        Task<List<Sponsor>> GetAll();
    }
}
