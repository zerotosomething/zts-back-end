﻿using Microsoft.EntityFrameworkCore;
using ZTS.Volley.Domain.Entities.Sponsors;
using ZTS.Volley.Infrastructure.Data;
using ZTS.Volley.Infrastructure.Repositories.Base;
using ZTS.Volley.Infrastructure.Repositories.Sponsors.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Sponsors
{
    public class SponsorRepository : BaseEntityRepository<Sponsor>, ISponsorRepository
    {
        public SponsorRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext) { }

        public async Task<Sponsor> GetByName(string name)
        {
            var sponsors = await applicationDbContext.Sponsors
                .Where(sponsor => sponsor.IsDeleted == false)
                .FirstOrDefaultAsync(sponsor => sponsor.Name == name);

            return sponsors;
        }

        public async Task<List<Sponsor>> GetAll()
        {
            var sponsors = await applicationDbContext.Sponsors
                .Where(sponsor => sponsor.IsDeleted == false)
                .ToListAsync();

            return sponsors;
        }
    }
}
