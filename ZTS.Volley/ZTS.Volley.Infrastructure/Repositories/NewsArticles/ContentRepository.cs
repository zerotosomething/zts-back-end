﻿using Microsoft.EntityFrameworkCore;
using ZTS.Volley.Domain.Entities.NewsArticles;
using ZTS.Volley.Infrastructure.Data;
using ZTS.Volley.Infrastructure.Repositories.Base;
using ZTS.Volley.Infrastructure.Repositories.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.NewsArticles
{
    public class ContentRepository : BaseEntityRepository<Content>, IContentRepository
    {
        public ContentRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<List<Content>> CreateForNews(News news, List<string> contentData, int contentTypeId, ContentType contentType)
        {
            var contents = new List<Content>();
            
            foreach (var content in contentData)
            {
                contents.Add(new Content
                {
                    Data = content,
                    IsDeleted = false,
                    ContentTypeId = contentTypeId,
                    ContentType = contentType,
                    News = news,
                    NewsId = news.Id
                });
            }

            await dbSet.AddRangeAsync(contents);
            await applicationDbContext.SaveChangesAsync();

            return contents;
        }

        public async Task<List<Content>> GetAllForNews(Guid id)
        {
            return await dbSet.Where(content => content.NewsId == id).ToListAsync();
        }
    }
}
