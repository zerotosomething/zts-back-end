﻿using ZTS.Volley.Domain.Entities.NewsArticles;
using ZTS.Volley.Infrastructure.Repositories.Base.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Interfaces
{
    public interface IContentRepository : IBaseEntityRepository<Content>
    {
        Task<List<Content>> CreateForNews(News news, List<string> contentData, int contentTypeId, ContentType contentType);
        Task<List<Content>> GetAllForNews(Guid id);
    }

}
