﻿using ZTS.Volley.Domain.Entities.NewsArticles;
using ZTS.Volley.Infrastructure.Repositories.Base.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Interfaces
{
    public interface INewsRepository : IBaseEntityRepository<News>
    {
    }
}
