﻿using ZTS.Volley.Domain.Entities.NewsArticles;
using ZTS.Volley.Infrastructure.Data;
using ZTS.Volley.Infrastructure.Repositories.Base;
using ZTS.Volley.Infrastructure.Repositories.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.NewsArticles
{
    public class ContentTypeRepository : BaseEnumEntityRepository<ContentType>, IContentTypeRepository
    {
        public ContentTypeRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext) { }

    }
}
