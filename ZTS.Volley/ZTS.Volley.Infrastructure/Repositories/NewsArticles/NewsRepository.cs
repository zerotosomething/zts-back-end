﻿using Microsoft.EntityFrameworkCore;
using ZTS.Volley.Domain.Entities.NewsArticles;
using ZTS.Volley.Infrastructure.Data;
using ZTS.Volley.Infrastructure.Repositories.Base;
using ZTS.Volley.Infrastructure.Repositories.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.NewsArticles
{
    public class NewsRepository : BaseEntityRepository<News>, INewsRepository
    {
        public NewsRepository(ApplicationDbContext context) : base(context) { }

        public async override Task<List<News>> GetAll()
        {
            return await dbSet.Where(news => news.IsDeleted == false).ToListAsync();
        }
    }
}
