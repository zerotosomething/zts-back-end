﻿namespace ZTS.Volley.Infrastructure.Repositories.Base.Interfaces
{
    public interface IBaseEntityRepository<TEntity>
    {
        Task<TEntity> Create(TEntity entity);
        Task<TEntity?> GetById(Guid id);
        Task<List<TEntity>> GetAll();
        Task<List<TEntity>> GetById(List<Guid> ids);
        Task<List<TEntity>> GetAllByDeletedStatus(bool isDeleted);
        Task<TEntity> Update(TEntity entity);
        Task Delete(TEntity entity);
    }
}
