﻿namespace ZTS.Volley.Infrastructure.Repositories.Base.Interfaces
{
    public interface IBaseEnumEntityRepository<TEntity>
    {
        Task<TEntity> Create(TEntity entity);
        Task<TEntity?> GetById(int id);
        Task<List<TEntity>> GetAll();
        Task<List<TEntity>> GetById(List<int> ids);
        Task<TEntity> Update(TEntity entity);
        Task Delete(TEntity entity);
    }

}
