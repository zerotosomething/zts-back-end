﻿using Microsoft.EntityFrameworkCore;
using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Infrastructure.Data;
using ZTS.Volley.Infrastructure.Repositories.Base;
using ZTS.Volley.Infrastructure.Repositories.Staff.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Staff
{
    public class StaffMemberRepository : BaseEntityRepository<StaffMember>, IStaffMemberRepository
    {
        public StaffMemberRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext) { }

        public override async Task<List<StaffMember>> GetAll()
        {
            var staffMembers = await applicationDbContext.StaffMember
                .Where(staffMember => staffMember.IsDeleted == false)
                .Include(staffMember => staffMember.StaffType)
                .Include(staffMember => staffMember.AgeCategory)
                .Include(staffMember => staffMember.StaffPositionHistories.Where(positionHistory => positionHistory.IsDeleted == false))
                    .ThenInclude(staffPosition => staffPosition.Position)
                .ToListAsync();

            return staffMembers;
        }

        public async Task<List<StaffMember>> GetByName(string name)
        {
            var staffMembers = await applicationDbContext.StaffMember
                .Where(staffMember => staffMember.IsDeleted == false)
                .Where(x => x.FirstName.ToUpper().Contains(name.ToUpper()) || x.LastName.ToUpper().Contains(name.ToUpper()))
                .Include(staffMember => staffMember.StaffType)
                .Include(staffMember => staffMember.AgeCategory)
                .Include(staffMember => staffMember.StaffPositionHistories.Where(positionHistory => positionHistory.IsDeleted == false))
                    .ThenInclude(staffPosition => staffPosition.Position)
                .ToListAsync();

            return staffMembers;
        }

        public override async Task<StaffMember?> GetById(Guid id)
        {
            var staffMember = await applicationDbContext.StaffMember
                .Where(staffMember => staffMember.Id == id)
                .Include(staffMember => staffMember.StaffType)
                .Include(staffMember => staffMember.AgeCategory)
                .Include(staffMember => staffMember.StaffPositionHistories.Where(positionHistory => positionHistory.IsDeleted == false).OrderBy(positionHistory => positionHistory.StartTime))
                    .ThenInclude(staffTrophies => staffTrophies.StaffTrophyPositionHistories.Where(stph => stph.StaffTrophy.IsDeleted == false).OrderBy(stph => stph.AcquiredDate))
                        .ThenInclude(positionTtrophy => positionTtrophy.StaffTrophy)
                .Include(staffMember => staffMember.StaffPositionHistories.Where(positionHistory => positionHistory.IsDeleted == false).OrderBy(positionHistory => positionHistory.StartTime))
                    .ThenInclude(staffPosition => staffPosition.Position)
                .FirstOrDefaultAsync();

            return staffMember;
        }
    }
}
