﻿using Microsoft.EntityFrameworkCore;
using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Infrastructure.Data;
using ZTS.Volley.Infrastructure.Repositories.Base;
using ZTS.Volley.Infrastructure.Repositories.Staff.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Staff
{
    public class PositionRepository : BaseEnumEntityRepository<Position>, IPositionRepository
    {
        public PositionRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext) { }

        public async Task<Position> GetByName(string name)
        {
            var position = await applicationDbContext.Position
                .Include(position => position.StaffPositionHistories)
                .FirstOrDefaultAsync(position => position.Name == name);

            return position;
        }
    }
}
