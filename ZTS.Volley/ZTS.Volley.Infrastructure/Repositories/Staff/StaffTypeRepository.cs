﻿using Microsoft.EntityFrameworkCore;
using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Infrastructure.Data;
using ZTS.Volley.Infrastructure.Repositories.Base;
using ZTS.Volley.Infrastructure.Repositories.Staff.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Staff
{
    public class StaffTypeRepository : BaseEnumEntityRepository<StaffType>, IStaffTypeRepository
    {
        public StaffTypeRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext) { }

        public async Task<StaffType> GetByName(string name)
        {
            var staffType = await applicationDbContext.StaffType
                .Include(staffType => staffType.StaffMembers)
                .FirstOrDefaultAsync(staffType => staffType.Name == name);

            return staffType;
        }
    }
}
