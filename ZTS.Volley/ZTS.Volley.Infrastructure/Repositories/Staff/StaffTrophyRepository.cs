﻿using Microsoft.EntityFrameworkCore;
using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Infrastructure.Data;
using ZTS.Volley.Infrastructure.Repositories.Base;
using ZTS.Volley.Infrastructure.Repositories.Staff.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Staff
{
    public class StaffTrophyRepository : BaseEntityRepository<StaffTrophy>, IStaffTrophyRepository
    {
        public StaffTrophyRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext) { }

        public async Task<List<StaffTrophy>> GetAll()
        {
            var staffTrophies = await applicationDbContext.StaffTrophy
                .Where(staffTrophy => staffTrophy.IsDeleted == false)
                .Include(staffTrophy => staffTrophy.StaffTrophyPositionHistories)
                .ToListAsync();

            return staffTrophies;
        }

        public async Task RemoveStaffTrophy(Guid staffPositionHistoryId, Guid trophyId)
        {
            var record = await applicationDbContext.StaffTrophyPositionHistory.Where(pth => pth.StaffTrophyId == trophyId &&
                                                                                            pth.StaffPositionHistoryId == staffPositionHistoryId).FirstOrDefaultAsync();

            applicationDbContext.StaffTrophyPositionHistory.Remove(record);
            await applicationDbContext.SaveChangesAsync();
        }
    }
}
