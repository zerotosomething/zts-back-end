﻿using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Infrastructure.Repositories.Base.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Staff.Interfaces
{
    public interface IStaffTrophyRepository : IBaseEntityRepository<StaffTrophy>
    {
        Task<List<StaffTrophy>> GetAll();
        Task RemoveStaffTrophy(Guid staffPositionHistoryId, Guid trophyId);
    }
}
