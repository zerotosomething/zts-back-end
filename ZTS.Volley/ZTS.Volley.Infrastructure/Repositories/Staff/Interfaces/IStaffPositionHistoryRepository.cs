﻿using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Domain.Entities.Staff.JoiningEntities;
using ZTS.Volley.Infrastructure.Repositories.Base.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Staff.Interfaces
{
    public interface IStaffPositionHistoryRepository : IBaseEntityRepository<StaffPositionHistory>
    {
        Task<List<StaffPositionHistory>> GetAll();
    }
}
