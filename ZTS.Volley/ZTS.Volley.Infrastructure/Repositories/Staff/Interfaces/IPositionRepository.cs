﻿using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Infrastructure.Repositories.Base.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Staff.Interfaces
{
    public interface IPositionRepository : IBaseEnumEntityRepository<Position>
    {
        Task<Position> GetByName(string name);
    }
}
