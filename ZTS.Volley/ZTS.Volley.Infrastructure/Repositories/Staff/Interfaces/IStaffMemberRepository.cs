﻿using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Infrastructure.Repositories.Base.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Staff.Interfaces
{
    public interface IStaffMemberRepository : IBaseEntityRepository<StaffMember>
    {
        Task<List<StaffMember>> GetAll();
        Task<List<StaffMember>> GetByName(string name);
    }
}
