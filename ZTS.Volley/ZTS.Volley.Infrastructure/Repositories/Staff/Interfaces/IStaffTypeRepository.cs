﻿using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Infrastructure.Repositories.Base.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Staff.Interfaces
{
    public interface IStaffTypeRepository : IBaseEnumEntityRepository<StaffType>
    {
        Task<StaffType> GetByName(string name);
    }
}
