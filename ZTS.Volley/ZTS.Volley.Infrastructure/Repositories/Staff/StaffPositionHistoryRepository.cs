﻿using Microsoft.EntityFrameworkCore;
using ZTS.Volley.Domain.Entities.Staff;
using ZTS.Volley.Infrastructure.Data;
using ZTS.Volley.Infrastructure.Repositories.Base;
using ZTS.Volley.Infrastructure.Repositories.Staff.Interfaces;

namespace ZTS.Volley.Infrastructure.Repositories.Staff
{
    public class StaffPositionHistoryRepository : BaseEntityRepository<StaffPositionHistory>, IStaffPositionHistoryRepository
    {
        public StaffPositionHistoryRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext) { }

        public override async Task<List<StaffPositionHistory>> GetAll()
        {
            var staffPositionHistories = await applicationDbContext.StaffPositionHistory
                .Where(staffPositionHistories => staffPositionHistories.IsDeleted == false)
                .Include(staffPositionHistory => staffPositionHistory.StaffTrophyPositionHistories)
                    .ThenInclude(positionTrophy => positionTrophy.StaffTrophy)
                .Include(staffPositionHistory => staffPositionHistory.Position)
                .ToListAsync();

            return staffPositionHistories;
        }

        public override async Task<StaffPositionHistory?> GetById(Guid id)
        {
            var staffPositionHistory = await applicationDbContext.StaffPositionHistory
                .Where(staffPosition => staffPosition.Id == id)
                .Include(staffPositionHistory => staffPositionHistory.StaffTrophyPositionHistories)
                    .ThenInclude(positionTrophy => positionTrophy.StaffTrophy)
                .Include(staffPositionHistory => staffPositionHistory.Position)
                .FirstOrDefaultAsync();

            return staffPositionHistory;
        }
    }
}
