using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.SwaggerUI;
using ZTS.Volley.API.ServiceExtensions;
using ZTS.Volley.Application.AutoMapper;
using ZTS.Volley.Application.Validators.Staff;
using ZTS.Volley.Infrastructure.Data;
using ZTS.Volley.Utils.Middlewares;

var builder = WebApplication.CreateBuilder(args);

builder.Services.ConfigureControllersOptions();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDatabases(builder.Configuration);
builder.Services.AddAutoMapper(typeof(MappingProfile).Assembly);
builder.Services.AddApiRepositories();
builder.Services.AddApiServices();
builder.Services.AddValidatorsFromAssemblyContaining<StaffMemberValidator>();
builder.Services.AddFluentValidationAutoValidation();

var corsSpecificOrigin = "AllowedOrigins";

builder.Services.AddCors(options =>
{
    options.AddPolicy(corsSpecificOrigin,
        policy =>
        {
            policy
                .AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod();
        });
});

var app = builder.Build();

app.UseSwagger();

app.UseSwaggerUI(opt =>
{
    opt.DocExpansion(DocExpansion.None);
    opt.EnableTryItOutByDefault();
});

using (var scope = app.Services.CreateScope())
{
    var dataContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
    dataContext.Database.Migrate();
}

app.UseHttpsRedirection();

app.UseCors(corsSpecificOrigin);

app.UseAuthorization();

app.UseMiddleware<ErrorHandlerMiddleware>();

app.MapControllers();

app.Run();
