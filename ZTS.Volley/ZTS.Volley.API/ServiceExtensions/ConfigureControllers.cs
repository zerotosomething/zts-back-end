﻿using System.Text.Json;

namespace ZTS.Volley.API.ServiceExtensions
{
    public static class ConfigureControllers
    {
        public static IServiceCollection ConfigureControllersOptions(this IServiceCollection services)
        {
            services.AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                });

            return services;
        }
    }
}
