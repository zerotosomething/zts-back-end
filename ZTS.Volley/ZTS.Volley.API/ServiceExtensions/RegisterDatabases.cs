﻿using Microsoft.EntityFrameworkCore;
using ZTS.Volley.Infrastructure.Data;

namespace ZTS.Volley.API.ServiceExtensions
{
    public static class RegisterDatabases
    {
        public static IServiceCollection AddDatabases(this IServiceCollection services, IConfiguration configuration)
        {
            var applicationConnectionString = configuration.GetConnectionString("ApplicationConnectionString");
            services.AddDbContext<ApplicationDbContext>(options => options.UseNpgsql(applicationConnectionString));

            return services;
        }
    }
}
