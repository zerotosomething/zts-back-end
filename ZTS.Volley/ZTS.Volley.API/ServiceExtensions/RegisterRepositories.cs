﻿using ZTS.Volley.Infrastructure.Repositories;
using ZTS.Volley.Infrastructure.Repositories.Club;
using ZTS.Volley.Infrastructure.Repositories.Club.Interfaces;
using ZTS.Volley.Infrastructure.Repositories.Interfaces;
using ZTS.Volley.Infrastructure.Repositories.Sponsors;
using ZTS.Volley.Infrastructure.Repositories.Sponsors.Interfaces;
using ZTS.Volley.Infrastructure.Repositories.Staff;
using ZTS.Volley.Infrastructure.Repositories.Staff.Interfaces;
using ZTS.Volley.Infrastructure.Repositories.NewsArticles;

namespace ZTS.Volley.API.ServiceExtensions
{
    public static class RegisterRepositories
    {
        public static IServiceCollection AddApiRepositories(this IServiceCollection services)
        {
            services.AddScoped<IAgeCategoryRepository, AgeCategoryRepository>();
            services.AddScoped<IPositionRepository, PositionRepository>();
            services.AddScoped<IStaffMemberRepository, StaffMemberRepository>();
            services.AddScoped<IStaffPositionHistoryRepository, StaffPositionHistoryRepository>();
            services.AddScoped<IStaffTrophyRepository, StaffTrophyRepository>();
            services.AddScoped<IStaffTypeRepository, StaffTypeRepository>();
            services.AddScoped<ISponsorRepository, SponsorRepository>();
            services.AddScoped<IMatchTypeRepository, MatchTypeRepository>();
            services.AddScoped<IMatchRepository, MatchRepository>();
            services.AddScoped<IClubTrophyRepository, ClubTrophyRepository>();
            services.AddScoped<IPastEventRepository, PastEventRepository>();
            services.AddScoped<IContentRepository, ContentRepository>();
            services.AddScoped<IContentTypeRepository, ContentTypeRepository>();
            services.AddScoped<INewsRepository, NewsRepository>();
            services.AddScoped<INewsStatusRepository, NewsStatusRepository>();

            return services;
        }
    }
}
