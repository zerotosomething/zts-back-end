﻿using ZTS.Volley.Application.Services;
using ZTS.Volley.Application.Services.Club;
using ZTS.Volley.Application.Services.Club.Interfaces;
using ZTS.Volley.Application.Services.Interfaces;
using ZTS.Volley.Application.Services.Sponsors;
using ZTS.Volley.Application.Services.Sponsors.Interfaces;
using ZTS.Volley.Application.Services.Staff;
using ZTS.Volley.Application.Services.Staff.Interfaces;
using ZTS.Volley.Application.Services.Interfaces.NewsArticles;
using ZTS.Volley.Application.Services.NewsArticles;
using ZTS.Volley.Application.Services.AzureStorage;

namespace ZTS.Volley.API.ServiceExtensions
{
    public static class RegisterServices
    {
        public static IServiceCollection AddApiServices(this IServiceCollection services)
        {
            services.AddScoped<IAgeCategoryService, AgeCategoryService>();
            services.AddScoped<IPositionService, PositionService>();
            services.AddScoped<IStaffMemberService, StaffMemberService>();
            services.AddScoped<IStaffPositionHistoryService, StaffPositionHistoryService>();
            services.AddScoped<IStaffTrophyService, StaffTrophyService>();
            services.AddScoped<IStaffTypeService, StaffTypeService>();
            services.AddScoped<ISponsorService, SponsorService>();
            services.AddScoped<IMatchTypeService, MatchTypeService>();
            services.AddScoped<IMatchService, MatchService>();
            services.AddScoped<IClubTrophyService, ClubTrophyService>();
            services.AddScoped<IPastEventService, PastEventService>();
            services.AddScoped<IContentService, ContentService>();
            services.AddScoped<IContentTypeService, ContentTypeService>();
            services.AddScoped<INewsService, NewsService>();
            services.AddScoped<INewsStatusService, NewsStatusService>();

            services.AddTransient<IAzureStorageService, AzureStorageService>();

            return services;
        }
    }
}
