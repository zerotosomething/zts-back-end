﻿using Microsoft.AspNetCore.Mvc;
using ZTS.Volley.Application.Dtos.Sponsors;
using ZTS.Volley.Application.Services.Sponsors.Interfaces;

namespace ZTS.Volley.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SponsorController : ControllerBase
    {
        private readonly ISponsorService sponsorService;

        public SponsorController(ISponsorService sponsorService)
        {
            this.sponsorService = sponsorService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(ResponseSponsorDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create(SponsorDto sponsorDto)
        {
            var sponsor = await sponsorService.Create(sponsorDto);

            return Ok(sponsor);
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseSponsorDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetById(Guid id)
        {
            var sponsor = await sponsorService.GetById(id);

            return Ok(sponsor);
        }

        [HttpGet]
        [Route("ids/")]
        [ProducesResponseType(typeof(List<ResponseSponsorDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetByIds([FromQuery] List<Guid> id)
        {
            var sponsors = await sponsorService.GetById(id);

            return sponsors.Any() ? Ok(sponsors) : NoContent();
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<ResponseSponsorDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
        {
            var sponsors = await sponsorService.GetAll();

            return Ok(sponsors);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(ResponseSponsorDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Update(Guid id, SponsorDto sponsorDto)
        {
            var sponsor = await sponsorService.Update(id, sponsorDto);

            return Ok(sponsor);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await sponsorService.Delete(id);

            return Ok(id);
        }
    }
}
