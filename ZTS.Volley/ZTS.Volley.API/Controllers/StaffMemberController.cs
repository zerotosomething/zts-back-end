﻿using Microsoft.AspNetCore.Mvc;
using ZTS.Volley.Application.Dtos.Staff.StaffMember;
using ZTS.Volley.Application.Services.Staff.Interfaces;

namespace ZTS.Volley.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StaffMemberController : ControllerBase
    {
        private readonly IStaffMemberService staffMemberService;

        public StaffMemberController(IStaffMemberService staffMemberService)
        {
            this.staffMemberService = staffMemberService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(ResponseStaffMemberDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create(StaffMemberDto staffMemberDto)
        {
            var staffMember = await staffMemberService.Create(staffMemberDto);

            return Ok(staffMember);
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseStaffMemberDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetById(Guid id)
        {
            var staffMember = await staffMemberService.GetById(id);

            return Ok(staffMember);
        }

        [HttpGet]
        [Route("ids/")]
        [ProducesResponseType(typeof(List<ResponseStaffMemberDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetByIds([FromQuery] List<Guid> id)
        {
            var staffMembers = await staffMemberService.GetById(id);

            return staffMembers.Any() ? Ok(staffMembers) : NoContent();
        }

        [HttpGet]
        [Route("name/")]
        [ProducesResponseType(typeof(List<ResponseStaffMemberDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetByName([FromQuery] string name)
        {
            var staffMembers = await staffMemberService.GetByName(name);

            return Ok(staffMembers);
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<ResponseStaffMemberDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
        {
            var staffMembers = await staffMemberService.GetAll();

            return Ok(staffMembers);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(ResponseStaffMemberDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Update(Guid id, StaffMemberDto staffMemberDto)
        {
            var staffMember = await staffMemberService.Update(id, staffMemberDto);

            return Ok(staffMember);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await staffMemberService.Delete(id);

            return Ok(id);
        }

        [HttpDelete("lastPosition/{staffId}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteLastPosition(Guid staffId)
        {
            var staffMemberHistory = await staffMemberService.DeleteLastPosition(staffId);

            return Ok(staffMemberHistory);
        }

        [HttpGet]
        [Route("positionHistory/")]
        [ProducesResponseType(typeof(StaffHistoryDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetPositionHistory(Guid id)
        {
            var positionHistory = await staffMemberService.GetPositionHistory(id);

            return Ok(positionHistory);
        }
    }
}
