﻿using Microsoft.AspNetCore.Mvc;
using ZTS.Volley.Application.Dtos;
using ZTS.Volley.Application.Services.Interfaces;

namespace ZTS.Volley.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AgeCategoryController : ControllerBase
    {
        private readonly IAgeCategoryService ageCategoryService;

        public AgeCategoryController(IAgeCategoryService ageCategoryService)
        {
            this.ageCategoryService = ageCategoryService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(ResponseAgeCategoryDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create(AgeCategoryDto ageCategoryDto)
        {
            var ageCategory = await ageCategoryService.Create(ageCategoryDto);

            return Ok(ageCategory);
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseAgeCategoryDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetById(int id)
        {
            var ageCategory = await ageCategoryService.GetById(id);

            return Ok(ageCategory);
        }

        [HttpGet]
        [Route("ids/")]
        [ProducesResponseType(typeof(List<ResponseAgeCategoryDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        public async Task<IActionResult> GetByIds([FromQuery] List<int> id)
        {
            var ageCategories = await ageCategoryService.GetById(id);

            return ageCategories.Any() ? Ok(ageCategories) : NoContent();
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<ResponseAgeCategoryDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
        {
            var ageCategories = await ageCategoryService.GetAll();

            return Ok(ageCategories);
        }

        [HttpPut]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseAgeCategoryDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Update(int id, AgeCategoryDto ageCategoryDto)
        {
            var ageCategory = await ageCategoryService.Update(id, ageCategoryDto);

            return Ok(ageCategory);
        }

        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(int id)
        {
            await ageCategoryService.Delete(id);

            return Ok(id);
        }
    }
}
