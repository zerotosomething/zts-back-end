﻿using Microsoft.AspNetCore.Mvc;
using ZTS.Volley.Application.Dtos.Staff.StaffType;
using ZTS.Volley.Application.Services.Staff.Interfaces;

namespace ZTS.Volley.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StaffTypeController : ControllerBase
    {
        private readonly IStaffTypeService staffTypeService;

        public StaffTypeController(IStaffTypeService staffTypeService)
        {
            this.staffTypeService = staffTypeService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(ResponseStaffTypeDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create(StaffTypeDto staffTypeDto)
        {
            var staffType = await staffTypeService.Create(staffTypeDto);

            return Ok(staffType);
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseStaffTypeDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetById(int id)
        {
            var staffType = await staffTypeService.GetById(id);

            return Ok(staffType);
        }

        [HttpGet]
        [Route("ids/")]
        [ProducesResponseType(typeof(List<ResponseStaffTypeDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetByIds([FromQuery] List<int> id)
        {
            var staffTypes = await staffTypeService.GetById(id);

            return staffTypes.Any() ? Ok(staffTypes) : NoContent();
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<ResponseStaffTypeDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetAll()
        {
            var staffTypes = await staffTypeService.GetAll();

            return Ok(staffTypes);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(ResponseStaffTypeDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Update(int id, StaffTypeDto staffTypeDto)
        {
            var staffType = await staffTypeService.Update(id, staffTypeDto);

            return Ok(staffType);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(int id)
        {
            await staffTypeService.Delete(id);

            return Ok(id);
        }
    }
}
