﻿using Microsoft.AspNetCore.Mvc;
using ZTS.Volley.Application.Dtos.Club.PastEvents;
using ZTS.Volley.Application.Services.Club.Interfaces;

namespace ZTS.Volley.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PastEventController : ControllerBase
    {
        private readonly IPastEventService pastEventService;

        public PastEventController(IPastEventService pastEventService)
        {
            this.pastEventService = pastEventService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(ResponsePastEventDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create(PastEventDto pastEventDto)
        {
            var pastEvent = await pastEventService.Create(pastEventDto);

            return Ok(pastEvent);
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponsePastEventDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetById(Guid id)
        {
            var pastEvent = await pastEventService.GetById(id);

            return Ok(pastEvent);
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<ResponsePastEventDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
        {
            var pastEvents = await pastEventService.GetAll();

            return Ok(pastEvents);
        }

        [HttpPut]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponsePastEventDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Update(Guid id, PastEventDto pastEventDto)
        {
            var pastEvent = await pastEventService.Update(id, pastEventDto);

            return Ok(pastEvent);
        }

        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await pastEventService.Delete(id);

            return Ok(id);
        }
    }
}
