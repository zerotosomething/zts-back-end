﻿using Microsoft.AspNetCore.Mvc;
using ZTS.Volley.Application.Dtos.Staff.Position;
using ZTS.Volley.Application.Services.Staff.Interfaces;

namespace ZTS.Volley.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PositionController : ControllerBase
    {
        private readonly IPositionService positionService;

        public PositionController(IPositionService positionService)
        {
            this.positionService = positionService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(ResponsePositionDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create(PositionDto positionDto)
        {
            var position = await positionService.Create(positionDto);

            return Ok(position);
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponsePositionDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetById(int id)
        {
            var position = await positionService.GetById(id);

            return Ok(position);
        }

        [HttpGet]
        [Route("ids/")]
        [ProducesResponseType(typeof(List<ResponsePositionDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetByIds([FromQuery] List<int> id)
        {
            var positions = await positionService.GetById(id);

            return positions.Any() ? Ok(positions) : NoContent();
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<ResponsePositionDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
        {
            var positions = await positionService.GetAll();

            return Ok(positions);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(ResponsePositionDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Update(int id, PositionDto positionDto)
        {
            var position = await positionService.Update(id, positionDto);

            return Ok(position);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(int id)
        {
            await positionService.Delete(id);

            return Ok(id);
        }
    }
}
