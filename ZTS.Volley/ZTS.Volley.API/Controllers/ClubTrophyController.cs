﻿using Microsoft.AspNetCore.Mvc;
using ZTS.Volley.Application.Dtos.Club.ClubTrophy;
using ZTS.Volley.Application.Services.Club.Interfaces;

namespace ZTS.Volley.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClubTrophyController : ControllerBase
    {
        private readonly IClubTrophyService clubTrophyService;

        public ClubTrophyController(IClubTrophyService clubTrophyService)
        {
            this.clubTrophyService = clubTrophyService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(ResponseClubTrophyDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create(ClubTrophyDto clubTrophyDto)
        {
            var clubTrophy = await clubTrophyService.Create(clubTrophyDto);

            return Ok(clubTrophy);
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseClubTrophyDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetById(Guid id)
        {
            var clubTrophy = await clubTrophyService.GetById(id);

            return Ok(clubTrophy);
        }

        [HttpGet]
        [Route("ids/")]
        [ProducesResponseType(typeof(List<ResponseClubTrophyDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetByIds([FromQuery] List<Guid> id)
        {
            var clubTrophies = await clubTrophyService.GetById(id);

            return clubTrophies.Any() ? Ok(clubTrophies) : NoContent();
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<ResponseClubTrophyDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetAll()
        {
            var clubTrophies = await clubTrophyService.GetAll();

            return Ok(clubTrophies);
        }

        [HttpPut]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseClubTrophyDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Update(Guid id, ClubTrophyDto clubTrophyDto)
        {
            var clubTrophy = await clubTrophyService.Update(id, clubTrophyDto);

            return Ok(clubTrophy);
        }

        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await clubTrophyService.Delete(id);

            return Ok(id);
        }
    }
}
