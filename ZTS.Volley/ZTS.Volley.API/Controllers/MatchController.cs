﻿using Microsoft.AspNetCore.Mvc;
using ZTS.Volley.Application.Dtos.Club.Match;
using ZTS.Volley.Application.Services.Club.Interfaces;

namespace ZTS.Volley.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MatchController : ControllerBase
    {
        private readonly IMatchService matchService;

        public MatchController(IMatchService matchService)
        {
            this.matchService = matchService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(ResponseMatchDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create(MatchDto matchDto)
        {
            var match = await matchService.Create(matchDto);

            return Ok(match);
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseMatchDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetById(Guid id)
        {
            var match = await matchService.GetById(id);

            return Ok(match);
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<ResponseMatchDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetAll()
        {
            var matches = await matchService.GetAll();

            return Ok(matches);
        }


        [HttpPut]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseMatchDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Update(Guid id, MatchDto matchDto)
        {
            var match = await matchService.Update(id, matchDto);

            return Ok(match);
        }
        
        [HttpPut]
        [Route("{id}/score")]
        [ProducesResponseType(typeof(ResponseMatchDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> UpdateScore(Guid id, UpdateMatchScoreDto matchDto)
        {
            var match = await matchService.UpdateScore(id, matchDto);

            return Ok(match);
        }

        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await matchService.Delete(id);

            return Ok(id);
        }
    }
}
