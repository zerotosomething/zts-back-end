﻿using Microsoft.AspNetCore.Mvc;
using ZTS.Volley.Application.Services.Staff.Interfaces;

namespace ZTS.Volley.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StaffPositionHistoryController : ControllerBase
    {
        private readonly IStaffPositionHistoryService staffPositionHistoryService;

        public StaffPositionHistoryController(IStaffPositionHistoryService staffPositionHistoryService)
        {
            this.staffPositionHistoryService = staffPositionHistoryService;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var staffPositionHistory = await staffPositionHistoryService.GetById(id);

            return Ok(staffPositionHistory);
        }

        [HttpGet]
        [Route("ids/")]
        public async Task<IActionResult> GetByIds([FromQuery] List<Guid> id)
        {
            var staffPositionHistories = await staffPositionHistoryService.GetById(id);

            return staffPositionHistories.Any() ? Ok(staffPositionHistories) : NoContent();
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var staffPositionHistories = await staffPositionHistoryService.GetAll();

            return Ok(staffPositionHistories);
        }
    }
}
