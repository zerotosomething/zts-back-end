﻿using Microsoft.AspNetCore.Mvc;
using ZTS.Volley.Application.Dtos.NewsArticles;
using ZTS.Volley.Application.Services.Interfaces.NewsArticles;

namespace ZTS.Volley.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ContentController : ControllerBase
    {
        readonly IContentService contentService;

        public ContentController(IContentService contentService)
        {
            this.contentService = contentService;
        }

        [HttpPost]
        public async Task<IActionResult> Add(ContentDto contentDto)
        {
            var content = await contentService.Create(contentDto);

            return Ok(content);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var content = await contentService.GetById(id);

            return Ok(content);
        }

        [HttpGet]
        [Route("ids/")]
        public async Task<IActionResult> GetByIds([FromQuery] List<Guid> id)
        {
            var content = await contentService.GetById(id);

            return content.Any() ? Ok(content) : NoContent();
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var content = await contentService.GetAll();

            return Ok(content);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(Guid id, ContentDto contentDto)
        {
            var content = await contentService.Update(id, contentDto);

            return Ok(content);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await contentService.Delete(id);

            return Ok(id);
        }
    }
}
