﻿using Microsoft.AspNetCore.Mvc;
using ZTS.Volley.Application.Dtos.NewsArticles;
using ZTS.Volley.Application.Services.Interfaces.NewsArticles;

namespace ZTS.Volley.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class NewsController : ControllerBase
    {
        readonly INewsService newsService;

        public NewsController(INewsService newsService)
        {
            this.newsService = newsService;
        }

        [HttpPost]
        public async Task<IActionResult> Add(CreateNewsRequestDto newsDto)
        {
            var response = await newsService.Create(newsDto);

            return Ok(response);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var news = await newsService.GetById(id);

            return Ok(news);
        }

        [HttpGet]
        [Route("ids/")]
        public async Task<IActionResult> GetByIds([FromQuery] List<Guid> id)
        {
            var news = await newsService.GetByIds(id);

            return news.Any() ? Ok(news) : NoContent();
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var news = await newsService.GetAll();

            return Ok(news);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(Guid id, CreateNewsRequestDto newsDto)
        {
            var news = await newsService.Update(id, newsDto);

            return Ok(news);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await newsService.Delete(id);

            return Ok(id);
        }
    }
}
