﻿using Microsoft.AspNetCore.Mvc;
using ZTS.Volley.Application.Dtos.NewsArticles;
using ZTS.Volley.Application.Services.Interfaces.NewsArticles;

namespace ZTS.Volley.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ContentTypeController : ControllerBase
    {
        readonly IContentTypeService contentTypeService;

        public ContentTypeController(IContentTypeService contentTypeService)
        {
            this.contentTypeService = contentTypeService;
        }

        [HttpPost]
        public async Task<IActionResult> Add(ContentTypeDto contentTypeDto)
        {
            var contentType = await contentTypeService.Create(contentTypeDto);

            return Ok(contentType);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var contentType = await contentTypeService.GetById(id);

            return Ok(contentType);
        }

        [HttpGet]
        [Route("ids/")]
        public async Task<IActionResult> GetByIds([FromQuery] List<int> id)
        {
            var contentType = await contentTypeService.GetById(id);

            return contentType.Any() ? Ok(contentType) : NoContent();
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var contentTypes = await contentTypeService.GetAll();

            return Ok(contentTypes);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, ContentTypeDto contentTypeDto)
        {
            var contentType = await contentTypeService.Update(id, contentTypeDto);

            return Ok(contentType);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await contentTypeService.Delete(id);

            return Ok(id);
        }
    }
}
