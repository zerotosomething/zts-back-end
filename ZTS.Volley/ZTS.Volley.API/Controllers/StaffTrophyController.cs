﻿using Microsoft.AspNetCore.Mvc;
using ZTS.Volley.Application.Dtos.Staff.StaffTrophy;
using ZTS.Volley.Application.Services.Staff.Interfaces;

namespace ZTS.Volley.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StaffTrophyController : ControllerBase
    {
        private readonly IStaffTrophyService staffTrophyService;

        public StaffTrophyController(IStaffTrophyService staffTrophyService)
        {
            this.staffTrophyService = staffTrophyService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(ResponseStaffTrophyDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create(StaffTrophyDto staffTrophyDto)
        {
            var staffTrophy = await staffTrophyService.Create(staffTrophyDto);

            return Ok(staffTrophy);
        }

        [HttpPost]
        [Route("staff")]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AddStaffTrophy(AddTrophyToStaffDto addTrophyToStaffDto)
        {
            await staffTrophyService.AddTrophyToStaff(addTrophyToStaffDto);

            return Ok(addTrophyToStaffDto.TrophyId);
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseStaffTrophyDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetById(Guid id)
        {
            var staffTrophy = await staffTrophyService.GetById(id);

            return Ok(staffTrophy);
        }

        [HttpGet]
        [Route("ids/")]
        [ProducesResponseType(typeof(List<ResponseStaffTrophyDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetByIds([FromQuery] List<Guid> id)
        {
            var staffTrophies = await staffTrophyService.GetById(id);

            return staffTrophies.Any() ? Ok(staffTrophies) : NoContent();
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<ResponseStaffTrophyDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
        {
            var staffTrophies = await staffTrophyService.GetAll();

            return Ok(staffTrophies);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(ResponseStaffTrophyDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Update(Guid id, StaffTrophyDto staffTrophyDto)
        {
            var staffTrophy = await staffTrophyService.Update(id, staffTrophyDto);

            return Ok(staffTrophy);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await staffTrophyService.Delete(id);

            return Ok(id);
        }

        [HttpDelete("{staffPositionHistoryId}&{trophyId}")]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RemoveTrophyFromStaff(Guid staffPositionHistoryId, Guid trophyId)
        {
            await staffTrophyService.Delete(staffPositionHistoryId, trophyId);

            return Ok(trophyId);
        }
    }
}
