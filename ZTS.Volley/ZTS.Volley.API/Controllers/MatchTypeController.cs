﻿using Microsoft.AspNetCore.Mvc;
using ZTS.Volley.Application.Dtos.Club;
using ZTS.Volley.Application.Services.Club.Interfaces;

namespace ZTS.Volley.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MatchTypeController : ControllerBase
    {
        private readonly IMatchTypeService matchTypeService;

        public MatchTypeController(IMatchTypeService matchTypeService)
        {
            this.matchTypeService = matchTypeService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(ResponseMatchTypeDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create(MatchTypeDto matchTypeDto)
        {
            var matchType = await matchTypeService.Create(matchTypeDto);

            return Ok(matchType);
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseMatchTypeDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetById(int id)
        {
            var matchType = await matchTypeService.GetById(id);

            return Ok(matchType);
        }

        [HttpGet]
        [Route("ids/")]
        [ProducesResponseType(typeof(List<ResponseMatchTypeDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ResponseMatchTypeDto), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetByIds([FromQuery] List<int> id)
        {
            var matchTypes = await matchTypeService.GetById(id);

            return matchTypes.Any() ? Ok(matchTypes) : NoContent();
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<ResponseMatchTypeDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
        {
            var matchTypes = await matchTypeService.GetAll();

            return Ok(matchTypes);
        }

        [HttpPut]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseMatchTypeDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Update(int id, MatchTypeDto matchTypeDto)
        {
            var matchType = await matchTypeService.Update(id, matchTypeDto);

            return Ok(matchType);
        }

        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(int id)
        {
            await matchTypeService.Delete(id);

            return Ok(id);
        }
    }
}
